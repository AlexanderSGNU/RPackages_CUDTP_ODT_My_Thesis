# classify circle with "in" and surrounding area with "out"

class1 <- function(x)
{
  if((x[1]-0.5)^2 + (x[2]-0.5)^2 <=0.16)
  {
    return("in")
  }
  else
  {
    return("out")
  }
}

exampleClassification1 = function(n)
{
  x = matrix(c(runif(n,0,1),runif(n,0,1)),ncol = 2,byrow=FALSE)
  y = vector(mode = "integer",length = n)
  for(index in 1:n)
  {
    y[index] <- class1(x[index,])
  }
  list(x,y)
}

library(Rcpp)
set.seed(1)
dtp_training_data <- exampleClassification1(1000)
dtp_object <- cudtp(X = dtp_training_data[[1]],
    Y = dtp_training_data[[2]],
    nbar = 500,
    max_depth = 4,
    max_subdivisions = 20,
    emptyLeafLabel = 1)
classList(dtp_object)
