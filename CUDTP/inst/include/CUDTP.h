#ifndef __CUDTP_H__
#define __CUDTP_H__

#include "./include_cudtp.h"
#include "./Bitvector.h"
#include "./Tree.h"
#include "./cubeCell.h"
#include "./labeledTree.h"
#include "./mpintVector.h"
#include "./dtp.h"

struct eta_obs
{
  long nObs;
  double eta;
  eta_obs(){}
  eta_obs(long nObs_, double eta_):
    nObs(nObs_), eta(eta_){}
};

class eta_gamma_tree
{
  public:
    long nObs;
    double eta;
    vector<double> gamma;
    vector<Tree> tree;
    eta_gamma_tree(const long & nObs_, const double& eta_,const vector<double>gamma_,vector<Tree> tree_):
      nObs(nObs_), eta(eta_), gamma(gamma_), tree(tree_) {}
    eta_gamma_tree(const eta_obs& eta_obs_, const vector<double>gamma_,vector<Tree> tree_):
      nObs(eta_obs_.nObs), eta(eta_obs_.eta), gamma(gamma_), tree(tree_) {}
};

cubeCell calculateParentCell(cubeCell cube);

cubeCell calculateChildFromIndex(cubeCell parent, size_t Index);

size_t calculateChildIndex(const cubeCell& child);

void sort(vector<cubeCell>& cubeCellVector);

IntegerVector predictWithTree(labeledTree tree, NumericMatrix& X);

labeledTree calculateAdaptiveDyadicTree(
    NumericMatrix& X1,
    NumericMatrix& X2,
    const IntegerVector& Y1,
    const IntegerVector& Y2,
    unsigned int max_depth,
    unsigned int max_subdivisions,
    unsigned int distinctLabels,
    unsigned int emptyLeafLabel);

double calculateAccuracy(
    labeledTree tree,
    NumericMatrix& X,
    const IntegerVector& Y);

#endif
