#ifndef __LEAF_H__
#define __LEAF_H__

#include "./Node.h"

class leaf: public Node
{
  public:
    bool isLeaf();
    virtual bool isFinalLeaf() = 0;
};

#endif
