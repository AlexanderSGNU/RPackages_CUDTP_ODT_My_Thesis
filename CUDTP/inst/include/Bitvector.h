#ifndef __BITVECTOR_H__
#define __BITVECTOR_H__

class Bitvector
{
private:
  size_t numberOfBits_;
  unsigned char* bitArray_;

  unsigned char charToHexChar(unsigned char c, bool pos) const;
  unsigned char HexCharsToChar(string str) const;
  unsigned char HexToChar(char c) const;

public:
  Bitvector();
  Bitvector(size_t numberOfBits, bool clear = false);
  Bitvector(unsigned char* c, size_t length);
  Bitvector(const Bitvector& bvec);
  Bitvector& operator=(const Bitvector& bvec);
  Bitvector& operator=(Bitvector&& bvec);
  Bitvector(Bitvector&& bvec);
  Bitvector(string& hex_array, size_t blength);
  ~Bitvector();
  size_t getNumberOfBits() const;
  bool getBit(size_t index) const;
  void setBit(size_t index);
  void clearBit(size_t index);
  void toggleBit(size_t index);
  void null_ptr();
  string toString();
  string toBitStream();
};

#endif
