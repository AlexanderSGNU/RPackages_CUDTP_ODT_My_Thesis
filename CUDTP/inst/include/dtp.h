#ifndef __DTP_H__
#define __DTP_H__

#include "./include_cudtp.h"
#include "./labeledTree.h"

class dtp
{
  public:
    dtp(NumericMatrix X,
        IntegerVector Y,
        unsigned int nbar = 0,
        unsigned int max_depth = 0,
        unsigned int max_subdivisions = 0,
        unsigned int distinctLabels = 0,
        unsigned int emptyLeafLabel = 0);
    IntegerVector predict(NumericMatrix& X);
    List cuboidList(unsigned int max_subdivisions);
    int getDim();
    IntegerVector classList(unsigned int max_subdivisions);
  private:
    labeledTree bestTree;
};

#endif
