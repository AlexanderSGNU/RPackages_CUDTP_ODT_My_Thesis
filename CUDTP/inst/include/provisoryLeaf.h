#ifndef __PROVISORYLEAF_H__
#define __PROVISORYLEAF_H__

#include "./include_cudtp.h"
#include "./leaf.h"

class provisoryLeaf: public leaf
{
  public:
    vector<unsigned int> labelCount_;
    provisoryLeaf(unsigned int distinctLabels);
    provisoryLeaf& operator= (const provisoryLeaf& pLeaf);
    provisoryLeaf& operator= (provisoryLeaf&& pLeaf);
    provisoryLeaf(const provisoryLeaf& pLeaf);
    provisoryLeaf(provisoryLeaf&& pLeaf);
    bool isFinalLeaf();
    void increase(size_t label);
    unsigned int calcMajorityLabel();
};

#endif
