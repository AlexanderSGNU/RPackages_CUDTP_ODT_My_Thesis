#ifndef __INNERNODE_H__
#define __INNERNODE_H__

#include "./include_cudtp.h"
#include "./cubeCell.h"
#include "./leaf.h"
#include "./Node.h"
#include "./innerNode.h"
#include "./finalLeaf.h"
#include "./provisoryLeaf.h"

class innerNode: public Node
{
  private:
    vector<Node*> childNodes_;
  public:
    innerNode(const vector<Node*>& childNodes_);
    innerNode(size_t size);
    ~innerNode();
    innerNode& operator= (const innerNode& iNode);
    innerNode& operator= (innerNode&& iNode);
    innerNode(const innerNode& iNode);
    innerNode(innerNode&& iNode);
    unsigned int getLabel(const NumericVector& x, cubeCell& node);
    void changeNode(Node* new_Node, size_t index);
    void setNode(Node* new_Node, size_t index);
    size_t getIndex(const NumericVector& x, cubeCell& node);
    void increase(const NumericVector& x, const unsigned int & y, cubeCell& node);
    bool isLeaf();
    void finalizeLeafs(unsigned int emptyLeafLabel);
    void cuboidList(vector<double>& vCList, cubeCell node);
    void classList(vector<int>& vCList, cubeCell node);
    double getCoordLow(size_t &child_index, unsigned int &dim_ind,cubeCell& node);
    double getCoordHigh(size_t &child_index, unsigned int &dim_ind,cubeCell& node);
};

#endif
