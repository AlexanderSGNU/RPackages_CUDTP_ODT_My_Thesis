#ifndef __NODE_H__
#define __NODE_H__

class Node
{
  public:
    virtual bool isLeaf() =0;
};

#endif
