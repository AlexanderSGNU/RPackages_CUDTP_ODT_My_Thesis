#ifndef __MPINTVECTOR_H__
#define __MPINTVECTOR_H__

#include "./include_cudtp.h"

class mpintVector
{
  public:
    vector<mpz_class> v;
    mpintVector();
    mpintVector(const vector<mpz_class>& vec);
    mpintVector(const mpintVector& vec);
    mpintVector(const IntegerVector& vec);
    mpintVector(size_t numberOfElements);
    mpintVector& operator=(const mpintVector& vec);
    mpintVector& operator=(mpintVector&& vec);
    mpintVector(mpintVector&& vec);
    mpintVector operator +(const mpintVector& v2) const;
    mpintVector operator +(mpz_class add) const;
    mpintVector operator -(mpz_class sub) const;
    mpintVector operator -(const mpintVector& v2) const;
    mpintVector& operator += (const mpintVector& v2);
    mpintVector& operator -= (const mpintVector& v2);
    bool operator== (const mpintVector& vec) const;
    bool operator <(const double& number) const;
    bool operator <(const mpintVector& sVec) const;
    bool operator <=(const mpintVector& vec) const;
    size_t size()  const;
    mpz_class& operator[](size_t index);
    const mpz_class operator()(size_t index) const;
    IntegerVector ToIntegerVector() const;
};

mpintVector getMinimalCell(const NumericVector& Xi, unsigned int d, int depth);

#endif
