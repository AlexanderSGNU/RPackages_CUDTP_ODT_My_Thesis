#ifndef __TREE_H__
#define __TREE_H__

#include "./cubeCell.h"
#include "./Bitvector.h"

// complete Tree
class Tree
{
  protected:
    int dimension_;
    Bitvector splits_;
  public:
    Tree();
    Tree(Bitvector splits, int dimension);
    Tree(unsigned char* c, size_t length, int dimension);
    Tree(string& hex_array, size_t blength, int dimension);
    Tree(const vector<size_t>& child_indices,const vector<Tree>& trees);
    cubeCell calculateNode(int depth, size_t index);
    int getDimension() const;
    size_t calculateNodePosition(cubeCell node);
    const Bitvector& getBitvector() const;
    static bool isTree(const Bitvector& possibleTree, int dimension);
    string toString();
    string toBitStream();
};

#endif
