#ifndef __CUBECELL_H__
#define __CUBECELL_H__

#include "./include_cudtp.h"
#include "./mpintVector.h"

//' @param lowIndices = indexvector who identifies the node at the specified depth uniquely
//' @param depth_ = depth of the tree at the position of the node
class cubeCell
{
public:
  mpintVector lowIndices;
  int depth_;
  cubeCell();
  cubeCell(const vector<mpz_class>& lInd, int depth);
  cubeCell(const IntegerVector& lowIndices, int depth);
  cubeCell(const cubeCell& cC);
  cubeCell(const NumericVector& Xi, unsigned int d, int depth);
  cubeCell& operator= (const cubeCell& cC);
  bool operator== (const cubeCell& cC) const;
  bool operator!= (const cubeCell& cC) const;
  bool operator< (const cubeCell& cC) const;
  size_t getDimension() const;
  int getDepth() const;
  List toList() const;
  static List toList(const cubeCell& cub);
};

#endif
