#ifndef __FINALLEAF_H__
#define __FINALLEAF_H__

#include "./leaf.h"

class finalLeaf: public leaf
{
  private:
    unsigned int label_;
  public:
    finalLeaf(unsigned int label);
    finalLeaf& operator= (const finalLeaf& fLeaf);
    finalLeaf& operator= (finalLeaf&& fLeaf);
    finalLeaf(const finalLeaf& fLeaf);
    finalLeaf(finalLeaf&& fLeaf);
    unsigned int getLabel() const;
    void setLabel(unsigned int label);
    bool isFinalLeaf();
};

#endif
