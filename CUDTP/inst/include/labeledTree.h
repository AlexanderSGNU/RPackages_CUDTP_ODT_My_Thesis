#ifndef __LABELEDTREE_H__
#define __LABELEDTREE_H__

#include "./include_cudtp.h"
#include "./cubeCell.h"
#include "./leaf.h"
#include "./Node.h"
#include "./innerNode.h"
#include "./finalLeaf.h"
#include "./provisoryLeaf.h"

class labeledTree
{
  private:
    int dimension_;
    Node * root_;
  public:
    labeledTree(const Tree& tree,
                NumericMatrix& X,
                const IntegerVector& Y,
                unsigned int distinctLabels,
                unsigned int emptyLeafLabel);
    labeledTree(const Tree& tree, vector<unsigned int>& labels);
    labeledTree();
    labeledTree& operator= (const labeledTree& ltree);
    labeledTree& operator= (labeledTree&& ltree);
    labeledTree(const labeledTree& ltree);
    labeledTree(labeledTree&& ltree);
    int getDimension() const;
    Node* getRoot() const;
    string toString();
    ~labeledTree();
    void increase(const NumericVector& X, const unsigned int & Y);
    void cuboidList(vector<double>& vCList);
    void classList(vector<int>& vCList);
    unsigned int getLabel(const NumericVector& x);
    void finalizeLeafs(unsigned int emptyLeafLabel);
};

#endif
