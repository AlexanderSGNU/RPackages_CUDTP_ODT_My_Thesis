#include "../inst/include/CUDTP.h"

Bitvector::Bitvector(): Bitvector(0) {}

Bitvector::Bitvector(size_t numberOfBits, bool clear): numberOfBits_(numberOfBits)
{
  if(numberOfBits%8 > 0)
  {
    bitArray_ = new unsigned char[numberOfBits/8+1];
    if(clear)
    {
      for(size_t pos = 0; pos < numberOfBits/8+1; pos++)
        bitArray_[pos] = 0;
    }
  }
  else
  {
    bitArray_ = new unsigned char[numberOfBits/8];
    if(clear)
    {
      for(size_t pos = 0; pos < numberOfBits/8; pos++)
        bitArray_[pos] = 0;
    }
  }
}

Bitvector::Bitvector(unsigned char* c, size_t length) : numberOfBits_(length*8)
{
  bitArray_ = new unsigned char[length];
  for(size_t i = 0; i < length;i++)
    bitArray_[i] = c[i];
}

Bitvector::Bitvector(const Bitvector& bvec): numberOfBits_(bvec.numberOfBits_)
{
  size_t length = numberOfBits_/8+ (numberOfBits_ % 8== 0 ? 0 : 1);
  bitArray_ = new unsigned char[length];
  for(size_t i = 0; i < length;i++)
    bitArray_[i] = bvec.bitArray_[i];
}

Bitvector& Bitvector::operator=(const Bitvector& bvec)
{
  numberOfBits_ = bvec.numberOfBits_;
  delete bitArray_;
  size_t length = numberOfBits_/8+ (numberOfBits_ % 8 == 0 ? 0 : 1);
  bitArray_ = new unsigned char[length];
  for(size_t i = 0; i < length;i++)
    bitArray_[i] = bvec.bitArray_[i];
  return *this;
}

Bitvector& Bitvector::operator=(Bitvector&& bvec)
{
  numberOfBits_ = bvec.numberOfBits_;
  delete bitArray_;
  bitArray_ = bvec.bitArray_;
  bvec.null_ptr();
  return *this;
}

Bitvector::Bitvector(Bitvector&& bvec): numberOfBits_(bvec.numberOfBits_), bitArray_(bvec.bitArray_){bvec.bitArray_ = nullptr;}

//' @title Bitvector
//' @description constructor
//' @param hex_array = array of strings with 2 chars with values of between 0-9 and a-f
//' @param blength = number of bits
Bitvector::Bitvector(string& hex_array, size_t blength): numberOfBits_(blength)
{
  size_t length = numberOfBits_/8+ (numberOfBits_ % 8 == 0 ? 0 : 1);
  bitArray_ = new unsigned char[length];
  for(size_t i = 0; i < length;i++)
    bitArray_[i] = HexCharsToChar(hex_array.substr(2*i,2));
}

size_t Bitvector::getNumberOfBits() const
{
  return numberOfBits_;
}

void Bitvector::null_ptr()
{
  bitArray_ = nullptr;
}

Bitvector::~Bitvector(){delete [] bitArray_;}

bool Bitvector::getBit(size_t index) const
{
  if(bitArray_[index/8] & (1<<(index%8)))
    return true;
  return false;
}

void Bitvector::setBit(size_t index)
{
  unsigned int charIndex = index/8;
  unsigned int remainder = index%8;
  bitArray_[charIndex] |=  1<<remainder;
}

void Bitvector::clearBit(size_t index)
{
  unsigned int charIndex = index/8;
  unsigned int remainder = index%8;
  bitArray_[charIndex] &=  ~(1<<remainder);
}

void Bitvector::toggleBit(size_t index)
{
  unsigned int charIndex = index/8;
  unsigned int remainder = index%8;
  bitArray_[charIndex] = ((bitArray_[charIndex] & 1<<remainder) ^ 1<<remainder) | (bitArray_[charIndex] & ~(1<<remainder)) ;
}

unsigned char Bitvector::charToHexChar(unsigned char c, bool pos) const
{
  if(pos)
    c &= 15;
  else
    c >>= 4;
  unsigned char charList[] = "0123456789abcdef";
  return charList[c];
}

//' description converts 2 chars (hexsymbols) to char
//' str needs to have length 2
unsigned char Bitvector::HexCharsToChar(string str) const
{
  return HexToChar(str[0]) << 4 | HexToChar(str[1]);
}

unsigned char Bitvector::HexToChar(char c) const
{
  switch(c)
  {
  case '0':
    return (unsigned char)(0);
  case '1':
    return (unsigned char)(1);
  case '2':
    return (unsigned char)(2);
  case '3':
    return (unsigned char)(3);
  case '4':
    return (unsigned char)(4);
  case '5':
    return (unsigned char)(5);
  case '6':
    return (unsigned char)(6);
  case '7':
    return (unsigned char)(7);
  case '8':
    return (unsigned char)(8);
  case '9':
    return (unsigned char)(9);
  case 'a':
    return (unsigned char)(10);
  case 'b':
    return (unsigned char)(11);
  case 'c':
    return (unsigned char)(12);
  case 'd':
    return (unsigned char)(13);
  case 'e':
    return (unsigned char)(14);
  case 'f':
    return (unsigned char)(15);
  }
  return (unsigned char)(0);
}

string Bitvector::toString()
{
  string result;
  int numberOfChars = numberOfBits_/8;
  if(numberOfBits_ %8 > 0)
    numberOfChars++;
  result.resize(numberOfChars*3);
  for(int i = 0; i < numberOfChars;i++)
  {
    for(int b = 0; b< 2;b++)
      result[i*3+b] = charToHexChar(bitArray_[i],b);
    result[i*3+2] = ' ';
  }
  return result;
}

string Bitvector::toBitStream()
{
  string str(numberOfBits_,'0');
  for(size_t index = 0; index < numberOfBits_;index++)
  {
    if(getBit(index))
      str[index] = '1';
  }
  return str;
}
