#include "../inst/include/CUDTP.h"

cubeCell::cubeCell()
  : lowIndices(mpintVector()), depth_(0)  {}

cubeCell::cubeCell(const vector<mpz_class>& lInd, int depth)
  : depth_(depth)
{
  lowIndices = mpintVector(lInd);
}

cubeCell::cubeCell(const IntegerVector& lInd, int depth)
  : lowIndices(mpintVector(lInd)), depth_(depth) {}

cubeCell::cubeCell(const cubeCell& cC)
{
  lowIndices = mpintVector(cC.lowIndices);
  depth_ = cC.getDepth();
}

// cell = root -> depth = 0
cubeCell::cubeCell(const NumericVector& Xi, unsigned int d, int depth)
  : lowIndices(getMinimalCell(Xi, d, depth)), depth_(depth){}

cubeCell& cubeCell::operator= (const cubeCell& cC)
{
  lowIndices = mpintVector(cC.lowIndices);
  depth_ = cC.getDepth();
  return *this;
}

bool cubeCell::operator== (const cubeCell& cC) const
{
  return (lowIndices==cC.lowIndices && depth_==cC.getDepth());
}

bool cubeCell::operator!= (const cubeCell& cC) const
{
  return !(*this == cC);
}

//' @description only for cells with same dimension and depth
bool cubeCell::operator<(const cubeCell& cC) const
{
  cubeCell c1(*this),c2(cC);
  unsigned int d = lowIndices.size();
  mpz_class c1lq,c2lq,nCells;
  unsigned int ind;
  for(int depth = c1.depth_-1; depth >= 0; depth--)
  {
    mpz_ui_pow_ui(nCells.get_mpz_t(),2,depth);
    for(ind = 0; ind < d; ind++)
    {
      c1lq = c1.lowIndices(ind)/nCells;
      c1.lowIndices[ind] = c1.lowIndices(ind)%nCells;
      c2lq = c2.lowIndices(ind)/nCells;
      c2.lowIndices[ind] = c2.lowIndices(ind)%nCells;
      if(c1lq < c2lq)
        return true;
      if(c1lq > c2lq)
        return false;
    }
  }
  return false;
}

void sort(vector<cubeCell>& cubeCellVector)
{
  sort(cubeCellVector.begin(),cubeCellVector.end());
}

size_t cubeCell::getDimension() const
{
  return lowIndices.size();
}

Rcpp::List cubeCell::toList() const
{
  return Rcpp::List::create(lowIndices.ToIntegerVector(),getDepth());
}

Rcpp::List cubeCell::toList(const cubeCell& cub)
{
  return Rcpp::List::create(cub.lowIndices.ToIntegerVector(),cub.getDepth());
}

int cubeCell::getDepth() const
{
  return depth_;
}
