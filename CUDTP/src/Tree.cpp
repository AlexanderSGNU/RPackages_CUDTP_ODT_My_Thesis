#include "../inst/include/CUDTP.h"

Tree::Tree(): dimension_(0), splits_(Bitvector()) {}

Tree::Tree(unsigned char* c, size_t length, int dimension): dimension_(dimension), splits_(Bitvector(c,length))
{
  if(!isTree(splits_,dimension_))
    throw logic_error("no complete tree");
}

Tree::Tree(string& hex_array, size_t blength, int dimension): dimension_(dimension), splits_(Bitvector(hex_array,blength))
{
  if(!isTree(splits_,dimension_))
    throw logic_error("no complete tree");
}

//' @description merge trees
//' @param child_indices = vector of indices returned by calculateChildIndex for each tree
//' indices starting at 0
//' @param trees = vector of trees to merge together

Tree::Tree(const vector<size_t>& child_indices,const vector<Tree>& trees): dimension_(trees[0].dimension_)
{
  // 2^dimension
  size_t two_dim = (1ull << dimension_);
  // calculate size of the new tree
  // = sum of size of old trees + 2^dimension
  size_t tree_size = 0;

  for(const Tree & i: trees)
  {
    tree_size += i.splits_.getNumberOfBits();
  }
  tree_size += two_dim;

  // create empty Bitvector of special size
  // filled only with 0s? yes
  splits_ = Bitvector(tree_size,true);

  // fill Bitvector -> tree constructed

  size_t new_tree_pos = two_dim;
  vector<size_t> positions(child_indices.size(),0);
  // number of child nodes with subdivisions at current depth
  vector<int> n_nodes(child_indices.size(),0);

  // create first subdivision
  for(size_t index = 0; index < child_indices.size();index++)
  {
    // if tree contains more than a root
    if(trees[index].splits_.getNumberOfBits() != 0)
    {
      splits_.setBit(child_indices[index]);
      n_nodes[index] = 1;
    }
  }

  // starting at the root: copy Bitvectorparts at current depth to new tree
  int n_count;
  size_t end;
  size_t index;
  size_t add_pos;
  while(accumulate(n_nodes.begin(),n_nodes.end(),0) > 0)
  {
    for(index = 0; index < child_indices.size();index++)
    {
      n_count = 0;
      end = two_dim * (size_t)(n_nodes[index]);
      for(add_pos = 0;add_pos < end; add_pos++)
      {
        if(trees[index].splits_.getBit(positions[index]+add_pos))
        {
          splits_.setBit(new_tree_pos);
          n_count++;
        }
        new_tree_pos++;
      }
      n_nodes[index] = n_count;
      positions[index] += end;
    }
  }
}

// complete Tree
bool Tree::isTree(const Bitvector& possibleTree, int dimension)
{
  // dimension = 0 and no leaves -> empty tree
  // no bits -> no extra leaves, only root
  if(possibleTree.getNumberOfBits() == 0)
    return true;
  size_t bitIndex = 0, length = possibleTree.getNumberOfBits(), bitsToGo = (1u << dimension);
  if(possibleTree.getNumberOfBits()% (1u<<dimension))
  {
    return false;
  }
  else
  {
    while(bitIndex < length)
    {
      if(bitsToGo == 0)
        return false;
      if(possibleTree.getBit(bitIndex))
        bitsToGo += (1u << dimension);
      bitIndex++;
      bitsToGo--;
    }
    if(bitsToGo == 0)
      return true;
  }
  return false;
}

const Bitvector& Tree::getBitvector() const
{
  return splits_;
}

string Tree::toString()
{
  return splits_.toString().append(to_string(dimension_));
}

string Tree::toBitStream()
{
  return splits_.toBitStream().append(string(" ").append(to_string(dimension_)));
}

int Tree::getDimension() const
{
  return dimension_;
}
