#include "../inst/include/CUDTP.h"

mpintVector::mpintVector()
  : v(vector<mpz_class>()){}
mpintVector::mpintVector(const vector<mpz_class>& vec)
{
  size_t size = vec.size();
  v = vector<mpz_class>(size);
  for(size_t index = 0; index < size;index++)
  {
    v[index] = vec[index];
  }
}

mpintVector::mpintVector(const mpintVector& vec): v(vec.v){}
mpintVector::mpintVector(size_t numberOfElements): v(vector<mpz_class>(numberOfElements)){}

mpintVector::mpintVector(const IntegerVector& vec)
{
  size_t size = vec.size();
  v.resize(size);
  for(size_t index = 0; index < size;index++)
  {
    v[index] = mpz_class(vec(index));
  }
}

mpintVector& mpintVector::operator=(const mpintVector& vec)
{
  v = vector<mpz_class>(vec.v);
  return *this;
}

mpintVector& mpintVector::operator=(mpintVector&& vec)
{
  v = vector<mpz_class>(vec.v);
  return *this;
}

mpintVector::mpintVector(mpintVector&& vec)
{
  v = vector<mpz_class>(vec.v);
}

mpintVector mpintVector::operator +(const mpintVector& v2) const
{
  mpintVector vec = mpintVector(*this);
  vec += v2;
  return vec;
}

mpintVector mpintVector::operator +(mpz_class add) const
{
  mpintVector vec;
  size_t size = v.size();
  vec.v.resize(size);
  for(size_t index = 0; index < size; index++)
  {
    vec.v[index]=v[index] + add;
  }
  return vec;
}

mpintVector mpintVector::operator -(mpz_class sub) const
{
  mpintVector vec;
  size_t size = v.size();
  vec.v.resize(size);
  for(size_t index = 0; index < size; index++)
  {
    vec.v[index]=v[index]-sub;
  }
  return vec;
}

mpintVector mpintVector::operator -(const mpintVector& v2) const
{
  mpintVector vec = *this;
  vec -= v2;
  return vec;
}

mpintVector& mpintVector::operator += (const mpintVector& v2)
{
  int size = v2.size();
  for(int index = 0; index < size;index++)
  {
    v[index] += v2.v[index];
  }
  return *this;
}

mpintVector& mpintVector::operator -= (const mpintVector& v2)
{
  int size = v2.size();
  for(int index = 0; index < size;index++)
  {
    v[index] -= v2.v[index];
  }
  return *this;
}

bool mpintVector::operator== (const mpintVector& vec) const
{
  int size = this->size();
  for(int index = 0; index < size; index++)
  {
    if(v[index] != vec.v[index])
      return false;
  }
  return true;
}

bool mpintVector::operator <(const double& number) const
{
  for(mpz_class element:v)
  {
    if(mpz_cmp_d(element.get_mpz_t(),number) < 0)
      return true;
    if(mpz_cmp_d(element.get_mpz_t(),number) > 0)
      return false;
  }
  return false;
}

bool mpintVector::operator <(const mpintVector& sVec) const
{
  for(size_t index = 0; index < v.size();index++)
  {
    if(v[index] < sVec.v[index])
      return true;
    if(v[index] > sVec.v[index])
      return false;
  }
  return false;
}

// returns true if elementwise comparison returns true for every element

bool mpintVector::operator <=(const mpintVector& vec) const
{
  size_t size = v.size();
  for(size_t index = 0; index < size;index++)
  {
    if(v[index] > vec.v[index])
      return false;
  }
  return true;
}

size_t mpintVector::size()  const
{
  return v.size();
}

mpz_class& mpintVector::operator[](size_t index)
{
  return v[index];
}

const mpz_class mpintVector::operator()(size_t index) const
{
  return v[index];
}

Rcpp::IntegerVector mpintVector::ToIntegerVector() const
{
  vector<int> vecs;
  vecs.resize(v.size());
  for(size_t index = 0; index < v.size(); index++)
  {
    vecs[index] = mpz_get_ui(v[index].get_mpz_t());
  }
  return Rcpp::IntegerVector(vecs.begin(),vecs.end());
}

//' @title getMinimalCell
//' @description calculates the cell containing an observation pair
//' bi is vector of indices with dimension d
//' if Xi[index] == 1 special handling
//' @return cell bi which contains Xi,
//' @param Xi vector of dimension d describing the observation
//' @param depth number of edges from the resulting cell to the root

mpintVector getMinimalCell(const NumericVector& Xi, unsigned int d, int depth)
{
  mpintVector indexVec;
  size_t size = Xi.size();
  indexVec.v.resize(size);
  for(size_t index = 0; index < size;index++)
  {
    if(Xi(index) == 1.0)
    {
      // (2^depth) - 1
//       indexVec.v[index] = pow(2,depth)-1;
      // 2^depth
      mpz_mul_2exp(indexVec.v[index].get_mpz_t(),mpz_class(1).get_mpz_t(),depth);
      // - 1
      indexVec.v[index]--;
    }
    else
    {
      // floor( Xi[index] * (2^depth) )
//       indexVec.v[index] = floor(Xi(index)*pow(2,depth));
      // 2^depth
      mpz_mul_2exp(indexVec.v[index].get_mpz_t(),mpz_class(1).get_mpz_t(),depth);
      indexVec.v[index] = floor(mpf_class(Xi(index))*indexVec.v[index]);
    }
  }
  return indexVec;
}
