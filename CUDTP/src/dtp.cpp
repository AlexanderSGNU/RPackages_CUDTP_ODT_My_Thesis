#include "../inst/include/CUDTP.h"

dtp::dtp(NumericMatrix X,
         IntegerVector Y,
         unsigned int nbar,
         unsigned int max_depth,
         unsigned int max_subdivisions,
         unsigned int distinctLabels,
         unsigned int emptyLeafLabel)
{
  // max_depth -> no cell can be smaller than this value
  // max_subdivisions = |S_bar|
  int len = X.nrow();
  NumericMatrix X1(X(Range(0,nbar-1),_));
  NumericMatrix X2(X(Range(nbar,len-1),_));
  IntegerVector Y1(Y.begin(),Y.begin()+nbar);
  IntegerVector Y2(Y.begin()+nbar,Y.end());
  bestTree = calculateAdaptiveDyadicTree(X1,X2,Y1,Y2,max_depth,max_subdivisions,distinctLabels,emptyLeafLabel);
}

IntegerVector dtp::predict(NumericMatrix& X)
{
  return predictWithTree(bestTree, X);
}

// returns a List of vertices describing the partition
// d consecutive values form a vertex
// 2 vertices form a cuboid
// a partition consists of a list of cuboids
// nCuboids = approximate number of cuboids in this partition, to speed up process
List dtp::cuboidList(unsigned int max_subdivisions = 20)
{
  unsigned int dim = bestTree.getDimension();
  size_t nCuboids = 1+max_subdivisions*((2u << dim)-1);
  vector<double> vCList;
  vCList.reserve(2*dim*nCuboids);
  bestTree.cuboidList(vCList);
  unsigned int len = vCList.size()/(2*dim);
  List l(len);
  auto it = vCList.begin();
  unsigned int index = 0;
  while(index != len)
  {
    l[index] = NumericVector(it,it+2*dim);
    it = it+2*dim;
    index++;
  }
  return l;
}

int dtp::getDim()
{
  return bestTree.getDimension();
}

IntegerVector dtp::classList(unsigned int max_subdivisions = 20)
{
  unsigned int dim = bestTree.getDimension();
  size_t nCuboids = 1+max_subdivisions*((2u << dim)-1);
  vector<int> vCList;
  vCList.reserve(2*dim*nCuboids);
  bestTree.classList(vCList);
  return IntegerVector(vCList.begin(),vCList.end());
}
