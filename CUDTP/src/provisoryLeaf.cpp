#include "../inst/include/provisoryLeaf.h"

bool provisoryLeaf::isFinalLeaf()
{
  return false;
}

provisoryLeaf::provisoryLeaf(unsigned int distinctLabels)
  : labelCount_(vector<unsigned int>(distinctLabels,0)) {}

// for more overview
#define copyInstructionspLeaf                                  \
vector<unsigned int>labelCount_(pLeaf.labelCount_.size());     \
for(size_t index = 0; index < pLeaf.labelCount_.size();index++)\
{                                                              \
  labelCount_[index] = pLeaf.labelCount_[index];               \
}

provisoryLeaf& provisoryLeaf::operator= (const provisoryLeaf& pLeaf)
{
  this->~provisoryLeaf();
  copyInstructionspLeaf
  return *this;
}

provisoryLeaf& provisoryLeaf::operator= (provisoryLeaf&& pLeaf)
{
  this->~provisoryLeaf();
  copyInstructionspLeaf
  return *this;
}

provisoryLeaf::provisoryLeaf(const provisoryLeaf& pLeaf)
{
  copyInstructionspLeaf
}

provisoryLeaf::provisoryLeaf(provisoryLeaf&& pLeaf)
{
  copyInstructionspLeaf
}

unsigned int provisoryLeaf::calcMajorityLabel()
{
  size_t label = 0;
  // check can start at 1 -> compare elements 0 and 1 ...
  for(size_t index = 1; index < labelCount_.size();index++)
  {
    if(labelCount_[label] < labelCount_[index])
    {
      label = index;
    }
  }
  // no observation at this leaf
  if(label == 0 && labelCount_[label] == 0)
    return 0;
  // majority label, 1 added for different cases
  return label+1;
}

void provisoryLeaf::increase(size_t label)
{
  labelCount_[label-1]++;
}
