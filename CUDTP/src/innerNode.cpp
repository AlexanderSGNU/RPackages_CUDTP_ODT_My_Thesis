#include "../inst/include/innerNode.h"
#include "../inst/include/CUDTP.h"

bool innerNode::isLeaf()
{
  return false;
}

innerNode::innerNode(size_t size): childNodes_(vector<Node*>(size)){}

// for more overview
#define copyInstructionsInnerNode                              \
childNodes_ = vector<Node*>(iNode.childNodes_.size());         \
for(size_t index = 0; index < iNode.childNodes_.size();index++)\
{                                                              \
  if(iNode.childNodes_[index]->isLeaf())                       \
  {                                                            \
    if(((leaf*)iNode.childNodes_[index])->isFinalLeaf())       \
    {                                                          \
      childNodes_[index] = new finalLeaf(*((finalLeaf*)iNode.childNodes_[index]));\
    }                                                          \
    else                                                       \
    {                                                          \
      childNodes_[index] = new provisoryLeaf(*((provisoryLeaf*)iNode.childNodes_[index]));\
    }                                                          \
  }                                                            \
  else                                                         \
  {                                                            \
    childNodes_[index] = new innerNode(*((innerNode*)iNode.childNodes_[index]));\
  }                                                            \
}

innerNode& innerNode::operator= (const innerNode& iNode)
{
  this->~innerNode();
  copyInstructionsInnerNode
  return *this;
}

innerNode& innerNode::operator= (innerNode&& iNode)
{
  this->~innerNode();
  copyInstructionsInnerNode
  return *this;
}

innerNode::innerNode(const innerNode& iNode)
{
  copyInstructionsInnerNode
}

innerNode::innerNode(innerNode&& iNode)
{
  copyInstructionsInnerNode
}


unsigned int innerNode::getLabel(const NumericVector& x, cubeCell& node)
{
  size_t index = getIndex(x,node);
  if(childNodes_[index]->isLeaf())
    return ((finalLeaf*)childNodes_[index])->getLabel();
  node = calculateChildFromIndex(node, index);
  return ((innerNode*)childNodes_[index])->getLabel(x,node);
}

void innerNode::setNode(Node* new_Node, size_t index)
{
  childNodes_[index] = new_Node;
}
void innerNode::changeNode(Node* new_Node, size_t index)
{
  if(childNodes_[index]->isLeaf())
  {
    if(((leaf*)childNodes_[index])->isFinalLeaf())
    {
      ((finalLeaf*)childNodes_[index])->~finalLeaf();
    }
    else
    {
      ((provisoryLeaf*)childNodes_[index])->~provisoryLeaf();
    }
  }
  else
  {
    ((innerNode*)childNodes_[index])->~innerNode();
  }
  childNodes_[index] = new_Node;
}

size_t innerNode::getIndex(const NumericVector& x, cubeCell& node)
{
  size_t index = 0, size = x.size();
  mpq_class low;
  for(size_t i = 0; i < size;i++)
  {
    low.get_num() = node.lowIndices[i]*2+1;
    mpz_ui_pow_ui(low.get_den().get_mpz_t(),2u,node.depth_+1);
    if(low < x[i])
    {
      index |= 1u << i;
    }
  }
  return index;
}

double innerNode::getCoordLow(size_t &child_index, unsigned int &dim_ind,cubeCell& node)
{
  mpq_class low;
  // optimization and error possible (overflow)
  low.get_num() = node.lowIndices[dim_ind]*2+((child_index >> dim_ind) & 1u)%2;
  mpz_ui_pow_ui(low.get_den().get_mpz_t(),2u,node.depth_+1);
  mpq_canonicalize(low.get_mpq_t());
  return mpq_get_d(low.get_mpq_t());
}

double innerNode::getCoordHigh(size_t &child_index, unsigned int &dim_ind,cubeCell& node)
{
  mpq_class low;
  // optimization and error possible (overflow)
  low.get_num() = node.lowIndices[dim_ind]*2+((child_index >> dim_ind) & 1u)%2+1;
  mpz_ui_pow_ui(low.get_den().get_mpz_t(),2u,node.depth_+1);
  mpq_canonicalize(low.get_mpq_t());
  return mpq_get_d(low.get_mpq_t());
}

innerNode::~innerNode()
{
  for(size_t index = 0; index < childNodes_.size();index++)
  {
    if(childNodes_[index]->isLeaf())
    {
      if(((leaf*)childNodes_[index])->isFinalLeaf())
      {
        ((finalLeaf*)childNodes_[index])->~finalLeaf();
      }
      else
      {
        ((provisoryLeaf*)childNodes_[index])->~provisoryLeaf();
      }
    }
    else
    {
      ((innerNode*)childNodes_[index])->~innerNode();
    }
  }
}

void innerNode::increase(const NumericVector& x, const unsigned int & y, cubeCell& node)
{
  size_t index = getIndex(x,node);
  if(childNodes_[index]->isLeaf())
  {
    if(!((leaf*)childNodes_[index])->isFinalLeaf())
    {
      ((provisoryLeaf*)childNodes_[index])->increase(y);
    }
  }
  else
  {
    node = calculateChildFromIndex(node, index);
    ((innerNode*)childNodes_[index])->increase(x,y,node);
  }
}

void innerNode::finalizeLeafs(unsigned int emptyLeafLabel)
{
  for(size_t index = 0; index < childNodes_.size(); index++)
  {
    if(childNodes_[index]->isLeaf())
    {
      if(((leaf*)childNodes_[index])->isFinalLeaf())
      {
        throw logic_error("leaf has already been finalized");
      }
      else
      {
        unsigned int label = ((provisoryLeaf*)childNodes_[index])->calcMajorityLabel();
        ((provisoryLeaf*)childNodes_[index])->~provisoryLeaf();
        // if no observation at this leaf
        if(label == 0)
        {
          childNodes_[index] = new finalLeaf(emptyLeafLabel);
        }
        // if there is a majority label or a tie
        else
        {
          childNodes_[index] = new finalLeaf(label);
        }
      }
    }
    else
    {
      ((innerNode*)childNodes_[index])->finalizeLeafs(emptyLeafLabel);
    }
  }
}

void innerNode::classList(vector<int>& vCList, cubeCell node)
{
  for(size_t index_child = 0; index_child < childNodes_.size(); index_child++)
  {
    if(childNodes_[index_child]->isLeaf())
    {
      if(((leaf*)childNodes_[index_child])->isFinalLeaf())
      {
        vCList.push_back(((finalLeaf*)childNodes_[index_child])->getLabel());
      }
      else
      {
        throw logic_error("there is at least one provisory leaf.");
      }
    }
    else
    {
      ((innerNode*)childNodes_[index_child])->
        classList(vCList,
                  calculateChildFromIndex(node, index_child)
        );
    }
  }
}

void innerNode::cuboidList(vector<double>& vCList, cubeCell node)
{
  for(size_t index_child = 0; index_child < childNodes_.size(); index_child++)
  {
    if(childNodes_[index_child]->isLeaf())
    {
      for(unsigned int d_index = 0; d_index < node.getDimension();d_index++)
      {
        vCList.push_back(
          getCoordLow(index_child, d_index, node)
        );
      }
      for(unsigned int d_index = 0; d_index < node.getDimension();d_index++)
      {
        vCList.push_back(
          getCoordHigh(index_child, d_index, node)
        );
      }
    }
    else
    {
      ((innerNode*)childNodes_[index_child])->
        cuboidList(vCList,
                   calculateChildFromIndex(node, index_child)
        );
    }
  }
}
