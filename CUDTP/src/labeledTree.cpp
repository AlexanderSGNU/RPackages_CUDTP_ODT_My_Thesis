#include "../inst/include/CUDTP.h"

unsigned int labeledTree::getLabel(const NumericVector& x)
{
  if(root_->isLeaf())
    return ((finalLeaf*)(root_))->getLabel();
  cubeCell rootCell = cubeCell(vector<mpz_class>(x.size(),0),0);
  return ((innerNode*)(root_))->getLabel(x,rootCell);
}

int labeledTree::getDimension() const
{
  return dimension_;
}

Node* labeledTree::getRoot() const
{
  return root_;
}

// for more overview
#define copyInstructions dimension_ = ltree.getDimension();    \
  if(ltree.getRoot()->isLeaf())                                \
  {                                                            \
    if(((leaf*)ltree.getRoot())->isFinalLeaf())                \
    {                                                          \
      root_ = new finalLeaf(*((finalLeaf*)ltree.getRoot()));   \
    }                                                          \
    else                                                       \
    {                                                          \
      root_ = new provisoryLeaf(*((provisoryLeaf*)ltree.getRoot()));\
    }                                                          \
  }                                                            \
  else                                                         \
  {                                                            \
    root_ = new innerNode(*((innerNode*)ltree.getRoot()));     \
  }

// deep copy needed
labeledTree::labeledTree(const labeledTree& ltree)
{
  copyInstructions
}

// deep copy needed
labeledTree& labeledTree::operator= (const labeledTree& ltree)
{
  this->~labeledTree();
  copyInstructions
  return *this;
}

// deep copy needed
labeledTree::labeledTree(labeledTree&& ltree)
{
  copyInstructions
}

// deep copy needed
labeledTree& labeledTree::operator= (labeledTree&& ltree)
{
  this->~labeledTree();
  copyInstructions
  return *this;
}

labeledTree::~labeledTree()
{
  if(root_->isLeaf())
  {
    if(((leaf*)root_)->isFinalLeaf())
    {
      ((finalLeaf*)root_)->~finalLeaf();
    }
    else
    {
      ((provisoryLeaf*)root_)->~provisoryLeaf();
    }
  }
  else
  {
    ((innerNode*)root_)->~innerNode();
  }
}

void labeledTree::increase(const NumericVector& X, const unsigned int & Y)
{
  if(root_->isLeaf())
  {
    if(((leaf*)root_)->isFinalLeaf())
    {
    }
    else
    {
      ((provisoryLeaf*)root_)->increase(Y);
    }
  }
  else
  {
    vector<mpz_class> mpz_vec(X.size());
    cubeCell rootCell = cubeCell(vector<mpz_class>(X.size()),0);
    ((innerNode*)root_)->increase(X,Y,rootCell);
  }
}

labeledTree::labeledTree(): dimension_(0), root_(new finalLeaf(0)) {}

//'@param tree - treestructure to be filled
//'@param X
//'@param Y
//'@param distinctLabels - number of different labels in Y
//'@param emptyLeafLabel - value a leaf with no observation should get

labeledTree::labeledTree(const Tree& tree,
            NumericMatrix& X,
            const IntegerVector& Y,
            unsigned int distinctLabels,
            unsigned int emptyLeafLabel)
{
  dimension_ = tree.getDimension();
  // build structure

  if(tree.getBitvector().getNumberOfBits() == 0)
  {
    // tree is empty Tree
    if(dimension_ == 0)
      throw logic_error("empty Tree as labeledTree");
    // tree has only root -> root is a leaf
    root_ = new provisoryLeaf(distinctLabels);
  }
  else
  {
    size_t nNewCells = 1u << dimension_;
    root_ = new innerNode(nNewCells);
    deque<Node*> v1;
    deque<Node*> v2;

    deque<Node*>* vOld = &v1;
    deque<Node*>* vNew = &v2;
    deque<Node*>* vDummy;

    Node* dummy_node;

    size_t tree_pos = 0;

    vNew->push_back(root_);

    // iterate over whole tree

    while(vNew->size() != 0)
    {
      vDummy = vOld;
      vOld = vNew;
      vNew = vDummy;
      vNew-> clear();
      while(vOld->size() != 0)
      {
        for(size_t index = 0; index < nNewCells;index++)
        {
          if(tree.getBitvector().getBit(tree_pos))
          {
            dummy_node = new innerNode(nNewCells);
            ((innerNode*)vOld->front())->setNode(dummy_node,index);
            vNew->push_back(dummy_node);
          }
          else
          {
            ((innerNode*)vOld->front())->setNode(new provisoryLeaf(distinctLabels),index);
          }
          tree_pos++;
        }
        vOld->pop_front();
      }
    }
  }

  // insert observations until nbar is reached
  size_t nrow = X.nrow();
  for(size_t index = 0; index < nrow; index++)
  {
    increase(X.row(index),Y[index]);
  }

  // finalize leafs
  // -> iterate over whole tree

  finalizeLeafs(emptyLeafLabel);

}

void labeledTree::finalizeLeafs(unsigned int emptyLeafLabel)
{
  if(root_->isLeaf())
  {
    if(((leaf*)root_)->isFinalLeaf())
    {
      throw logic_error("leaf has already been finalized");
    }
    else
    {
      unsigned int label = ((provisoryLeaf*)root_)->calcMajorityLabel();
      ((provisoryLeaf*)root_)->~provisoryLeaf();
      // if no observation at this leaf
      if(label == 0)
      {
        root_ = new finalLeaf(emptyLeafLabel);
      }
      // if there is a majority label or a tie
      else
      {
        root_ = new finalLeaf(label);
      }
    }
  }
  else
  {
    ((innerNode*)root_)->finalizeLeafs(emptyLeafLabel);
  }
}

void labeledTree::cuboidList(vector<double>& vCList)
{
  if(root_->isLeaf())
  {
    for(int index = 0; index < dimension_;index++)
    {
      vCList.push_back(0);
    }
    for(int index = 0; index < dimension_;index++)
    {
      vCList.push_back(1);
    }
  }
  else
  {
    ((innerNode*)root_)->cuboidList(vCList, cubeCell(vector<mpz_class>(dimension_,0),0));
  }
}

void labeledTree::classList(vector<int>& vCList)
{
  if(root_->isLeaf())
  {
    if(((leaf*)root_)->isFinalLeaf())
    {
      vCList.push_back(((finalLeaf*)root_)->getLabel());
    }
    else
    {
      throw logic_error("there are still provisory leafs.");
    }
  }
  else
  {
    ((innerNode*)root_)->classList(vCList, cubeCell(vector<mpz_class>(dimension_,0),0));
  }
}
