//' @useDynLib CUDTP
//' @importFrom Rcpp evalCpp

#include "../inst/include/CUDTP.h"

////' @title calculateParentCell
////' @description calculates the parent of child
////' @param child cubeCell which parent should be calculated
////' @return parent of child

cubeCell calculateParentCell(cubeCell child)
{
  int d = child.getDimension();
  // if there is a parent cell
  if(d > 0)
  {
    for(int i = 0; i < d; i++)
    {
      child.lowIndices[i] >>= 1u;
    }
    child.depth_--;
  }
  return child;
}

////' @title calculateChildIndex
////' @description calculates the index of the child in respect to its parent, 0 <= index < 2^d
////' @param child
////' @return index

size_t calculateChildIndex(const cubeCell& child)
{
  int d = child.getDimension();
  size_t result = 0;
  for(int i = 0; i < d;i++)
  {
    if(child.lowIndices(i)%2 != 0)
      result |= 1u << i;
  }
  return result;
}

////' @title calculateChildFromIndex
////' @description calculates the child of parent with index Index
////' @param parent parent of child
////' @param Index uniquely identifies the child cell
////' @return child

cubeCell calculateChildFromIndex(cubeCell parent, size_t Index)
{
  int d = parent.getDimension();
  for(int i = 0; i < d;i++)
  {
    parent.lowIndices[i] = (parent.lowIndices[i]<<1);
    // max(1u << i) = 2^32?
    if(Index & (1u << i))
      parent.lowIndices[i]++;
  }
  parent.depth_++;
  return parent;
}

eta_obs sum_eta(vector<eta_gamma_tree>::iterator begin, size_t n_children)
{
  double sum = 0;
  long nObs = 0;
  for(size_t index = n_children; index != 0; index--)
  {
    sum += (begin->eta);
    nObs += begin->nObs;
    begin++;
  }
  return eta_obs(nObs,sum);
}

// calculates a vector with the maximum number of subdivisions for every child

vector<unsigned int> children_max_subdivisions(vector<eta_gamma_tree>::iterator begin, size_t n_children)
{
  vector<unsigned int> children_subdivisions(n_children);
  for(size_t index = 0; index != n_children; index++)
  {
    children_subdivisions[index] = begin->tree.size()-1;
    begin++;
  }
  return children_subdivisions;
}

double sum_energie(vector<eta_gamma_tree>::iterator begin,
                   const size_t& n_children,
                   const vector<int>& l_star_vec)
{
  double energie = 0;
  for(size_t j = 0; j < n_children; j++)
  {
    energie += begin->gamma[l_star_vec[j]];
    begin++;
  }
  return energie;
}

Tree mergeTrees(const vector<eta_gamma_tree>::iterator & egBegin,
                const vector<cubeCell>::iterator & uBegin,
                const vector<int> & l_star_vec)
{
  int dim = egBegin->tree[0].getDimension();
  string empty_tree = string("");
  vector<Tree> trees(l_star_vec.size(),Tree(empty_tree,0,dim));
  vector<size_t> childIndices(l_star_vec.size(),0);
  for(size_t index = 0; index < l_star_vec.size(); index++)
  {
    trees[index] = (egBegin+index)->tree[l_star_vec[index]];
    childIndices[index] = calculateChildIndex(*(uBegin+index));
  }
  return Tree(childIndices,trees);
}

////' @title argmax_l_star
////' @description calculates the best vector of child trees to use for creating a parent tree
////' the vector contains the number of subdivisions each child tree has
////' iterates over all possibilities
////' @param begin = iterator of eta_gamma_tree, so informations about the child trees can be retrieved
////' @param j = sum of subdivisions the children should have together
////' @param size = number of children
////' @return vector of subdivisions
vector<int> argmax_l_star(const vector<eta_gamma_tree>::iterator & begin,
                          const size_t & j,
                          const size_t & size)
{
  double max_gamma = -DBL_MAX,new_gamma;
  vector<int> max_int_vec, q_k(size);
  long cs[size+2];   // use vector instead? Warning: ISO C++ forbids variable length array
  // initialize cs array
  cs[size+1] = 0;
  cs[size] = j+size-1;
  unsigned long index = size-1;
  for(; index+1 != 0; index--)
  {
    cs[index] = index-1;
  }
  do
  {
    // set index to 1
    index = 1;
    // calculate q_k
    for(size_t ind = 0; ind != size; ind++)
    {
      q_k[ind] = (cs[ind+1]-cs[ind])-1;
    }
    new_gamma = sum_energie(begin,size,q_k);
    if(max_gamma < new_gamma)
    {
      max_int_vec = q_k;
      max_gamma = new_gamma;
    }
    while(cs[index] +1 == cs[index+1])
    {
      cs[index] = index-1;
      index++;
    }
    cs[index]++;
  } while (index < size);
  return max_int_vec;
}

////' @title argmax_l_star
////' @description calculates the best vector of child trees to use for creating a parent tree
////' the vector contains the number of subdivisions each child tree has
////' iterates over all possibilities
////' special case with number of subdivisions differ for each child
////' @param begin = iterator of eta_gamma_tree, so informations about the child trees can be retrieved
////' @param j = sum of subdivisions the children should have together
////' @param max_values = vector of number of subdivisions that are maximal possible for each child
////' @param size = number of children
////' @return vector of subdivisions
vector<int> argmax_l_star(const vector<eta_gamma_tree>::iterator & begin,
                          const size_t & j,
                          const vector<unsigned int> & max_values,
                          const size_t & size)
{
  double max_gamma = -DBL_MAX,new_gamma;
  vector<int> max_int_vec, q_k(size);
  long cs[size+2];   // use vector instead? Warning: ISO C++ forbids variable length array
  // initialize cs array
  cs[size+1] = 0;
  cs[size] = j+size-1;
  unsigned long index = size-1;
  for(; index+1 != 0; index--)
  {
    cs[index] = index-1;
  }
  bool l_star_out_of_range;
  do
  {
    // set index to 1
    index = 1;
    // calculate q_k
    l_star_out_of_range = false;
    for(size_t ind = 0; ind != size; ind++)
    {
      q_k[ind] = (cs[ind+1]-cs[ind])-1;
      if(q_k[ind] > max_values[ind])
      {
        l_star_out_of_range = true;
        break;
      }
    }
    // only if vector was valid, else skip these values for maximum calculation
    if(!l_star_out_of_range)
    {
      new_gamma = sum_energie(begin,size,q_k);
      if(max_gamma < new_gamma)
      {
        max_int_vec = q_k;
        max_gamma = new_gamma;
      }
    }
    while(cs[index] +1 == cs[index+1])
    {
      cs[index] = index-1;
      index++;
    }
    cs[index]++;
  } while (index < size);
  return max_int_vec;
}

IntegerVector predictWithTree(labeledTree tree, NumericMatrix& X)
{
  int rows = X.nrow();
  IntegerVector y = IntegerVector(rows);
  for(int index = 0; index < rows; index++)
  {
    y[index] = tree.getLabel(X.row(index));
  }
  return y;
}

double calculateAccuracy(
    labeledTree tree,
    NumericMatrix& X,
    const IntegerVector& Y)
{
  IntegerVector pred_y = predictWithTree(tree,X);
  int size = Y.size(), nCorrect = 0;
  for(int index = 0; index < size;index++)
  {
    if(Y[index] == pred_y[index])
      nCorrect++;
  }
  return ((double)nCorrect)/size;
}

////' @title calculateAdaptiveDyadicTree
////' @description grows nbar trees from cells of depth max_subdivisions with the first nbar observations of (X,Y)
////' use the rest of the observations to find the best performing tree on these observations and return it.
////' @param X1 = features for growing the trees
////' @param Y1 = labels for growing the trees
////' @param X2 = features for picking the best tree
////' @param Y2 = classlabels for picking the best tree
////' @param max_subdivisions = maximal number of max_subdivisions
////' @param distinctLabels = number of distinct labels
////' @param emptyLeafLabel = value a cell should get, if it is empty
////' @return labeledTree = dyadic tree describing the classifier
labeledTree calculateAdaptiveDyadicTree(
    NumericMatrix& X1,
    NumericMatrix& X2,
    const IntegerVector& Y1,
    const IntegerVector& Y2,
    unsigned int max_depth,
    unsigned int max_subdivisions,
    unsigned int distinctLabels,
    unsigned int emptyLeafLabel)
{

  // get number of dimensions

  unsigned int dim = X1.ncol();

  // get number of rows of X1

  unsigned int nbar = X1.nrow();

  // calculate the maximal possible depth of our tree

  unsigned int needed_depth = min(max_subdivisions,max_depth);

  // create U0 and sort it according to the position in the bitstream of the occupationtree

  vector<cubeCell> minList(nbar,cubeCell());
  vector<size_t> index_vec(nbar,0);
  for(size_t index = 0; index <nbar;index++)
  {
    // needed_depth instead of nbar
    minList[index] = cubeCell(X1.row(index),dim,needed_depth);
    index_vec[index] = index;
  }

  // index_vec contains sorting structure
  sort(index_vec.begin(),index_vec.end(),
       [&](const size_t& a, const size_t& b){
         return minList[a] < minList[b];
       });

  // create vector and pointer of occupied cubes with special resolution nbar - l, only old one and new one needed
  // improvement: special resolution needed_depth - l
  // uOldIt points at first to U_0

  vector<cubeCell> containerU1;
  vector<cubeCell> containerU2;
  vector<cubeCell> * uOldIt = &containerU1;
  vector<cubeCell> * uNewIt = &containerU2;
  vector<cubeCell> * uTempIt;

  containerU1.reserve(nbar);
  containerU2.reserve(nbar);

  // create container and pointer with important informations like eta, gamma, tree

  vector<eta_gamma_tree> egt1,egt2;
  vector<eta_gamma_tree> * egtOldIt = &egt1, * egtNewIt = &egt2, * egtTempIt;

  // initialization step l = 0

  // tree consisting only of root, if dimension is set
  string empty_tree = string("");
  size_t n_eta = 1;
  double bar_eta_Q;
  bar_eta_Q = (Y1(index_vec[0]) << 1)-3;
  containerU1.push_back(minList[index_vec[0]]);
  // combine maximal cells to smaller cells for our first U
  for(size_t index = 1; index < nbar;index++)
  {
    if(minList[index_vec[index]] != (*(--containerU1.end())))
    {
      containerU1.push_back(minList[index_vec[index]]);
      bar_eta_Q /= n_eta;
      egtOldIt->push_back(eta_gamma_tree(
          n_eta,
          bar_eta_Q,
          vector<double>(1,max(0.0,bar_eta_Q)),
          vector<Tree>(1,Tree(empty_tree,0,dim))
      ));
      // new calculation starts
      bar_eta_Q = (Y1(index_vec[index]) << 1)-3;
      n_eta = 1;
    }
    else
    {
      bar_eta_Q += (Y1(index_vec[index]) << 1)-3;
      n_eta++;
    }
  }
  bar_eta_Q /= n_eta;
  egtOldIt->push_back(eta_gamma_tree(
      n_eta,
      bar_eta_Q,
      vector<double>(1,max(0.0,bar_eta_Q)),
      vector<Tree>(1,Tree(empty_tree,0,dim))
  ));

  // main loop with calculations

  vector<size_t> parent_range;
  cubeCell parent;
  size_t n_children;
  vector<int> l_star_vec;
  vector<eta_gamma_tree>::iterator egBegin, egEnd;
  vector<cubeCell>::iterator uBegin;
  eta_obs e_o;
  // max_subdivisions instead of nbar
  // needed_depth instead of nbar
  // is there a problem if max_depth is too small? -> check in R
  for(size_t l = 1; l<= needed_depth;l++)
  {
    // calculate uNew
    parent_range.push_back(0);
    parent = calculateParentCell(uOldIt->at(0));
    uNewIt->push_back(parent);
    for(size_t index = 1; index < uOldIt->size();index++)
    {
      parent = calculateParentCell((*uOldIt)[index]);
      if(parent != *(--(uNewIt->end())))
      {
        uNewIt->push_back(parent);
        // save index, so no need for parent recalculation later, when occupied children needed.
        parent_range.push_back(index);
      }
    }
    parent_range.push_back(uOldIt->size());

    // calculate eta_gamma_tree for each Q in U_l

    for(size_t index = 0; index < uNewIt->size();index++)
    {
      // set iterators
      egBegin = egtOldIt->begin()+parent_range[index];
      uBegin = uOldIt->begin()+parent_range[index];

      // number of elements of C'(Q)
      n_children = parent_range[index+1]-parent_range[index];

      // j = 0 special case for eta_gamma_tree

      e_o = sum_eta(egBegin,n_children);
      egtNewIt->push_back(eta_gamma_tree(
          e_o,
          vector<double>(1,max(0.0,e_o.eta)),
          vector<Tree>(1,Tree(empty_tree,0,dim))
      ));

      // main part

      // j > 0

      // children_m_sub_vector contains the information
      // about the maximal number of subdivisions for every occupied child
      vector<unsigned int> children_m_sub_vector = children_max_subdivisions(egBegin,n_children);
      // saves the minimum value of children_m_sub_vector
      // is used to check if l_star_vec can be calculated normally
      size_t max_possible_subdivisions_child = *min_element(children_m_sub_vector.begin(),children_m_sub_vector.end());
      // this is equal to l in the original algorithm
      size_t loopend = l+max_subdivisions-needed_depth;
      for(size_t j = 1; j<=loopend;j++)
      {
        // l_star_vec = argmax(l_single_quote,sum_gamma,parent_range[index]))
        // j-1 because if we consider the cells of the children
        // then we've already had one subdivision
        if(loopend - max_possible_subdivisions_child == 1)
        {
          l_star_vec = argmax_l_star(egBegin,j-1,n_children);
        }
        else
        {
          // special case with different number of subdivisions for each child
          l_star_vec = argmax_l_star(egBegin,j-1,children_m_sub_vector,n_children);
          if(l_star_vec.size() == 0)
            break;
        }
        // gamma_q_j = sum_gamma(C'(Q),l_star_vec)
        (*egtNewIt)[index].gamma.push_back(sum_energie(
            egBegin,
            n_children,
            l_star_vec
        ));
        // add with preserved order!
        // tree_q_j = tree(only_q).add(rtrees).add(qs_not_occupied)
        (*egtNewIt)[index].tree.push_back(mergeTrees(
            egBegin,
            uBegin,
            l_star_vec
        ));
      }
    }

    // prepare for next loop iteration

    uTempIt = uOldIt;
    uOldIt = uNewIt;
    uNewIt = uTempIt;
    uNewIt->clear();

    egtTempIt = egtOldIt;
    egtOldIt = egtNewIt;
    egtNewIt = egtTempIt;
    egtNewIt->clear();
    parent_range.clear();
  }

    // choose best tree

  labeledTree bestTree, dummyTree;
  double bestAcc = 0, dummyAcc;

  for(auto it = (*egtOldIt)[0].tree.begin();it != (*egtOldIt)[0].tree.end();it++)
  {
    dummyTree = labeledTree(*it, X1, Y1, distinctLabels, emptyLeafLabel);
    dummyAcc = calculateAccuracy(dummyTree, X2, Y2);
    if(dummyAcc > bestAcc)
    {
      bestAcc = dummyAcc;
      bestTree = dummyTree;
    }
  }

  return bestTree;
}

// exporting class dtp to R

RCPP_MODULE(mod_dtp){
  class_<dtp>("dtp")

  .constructor<NumericMatrix, IntegerVector, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int>()

  .method("predict", &dtp::predict)
  .method("cuboidList", &dtp::cuboidList)
  .method("classList", &dtp::classList)
  .method("getDim", &dtp::getDim)
  ;
}
