#include "../inst/include/finalLeaf.h"

finalLeaf::finalLeaf(unsigned int label): label_(label){}

finalLeaf& finalLeaf::operator= (const finalLeaf& fLeaf)
{
  this->~finalLeaf();
  setLabel(fLeaf.getLabel());
  return *this;
}

finalLeaf& finalLeaf::operator= (finalLeaf&& fLeaf)
{
  this->~finalLeaf();
  setLabel(fLeaf.getLabel());
  return *this;
}

finalLeaf::finalLeaf(const finalLeaf& fLeaf): label_(fLeaf.label_){}

finalLeaf::finalLeaf(finalLeaf&& fLeaf): label_(fLeaf.label_){}

unsigned int finalLeaf::getLabel() const
{
  return label_;
}

void finalLeaf::setLabel(unsigned int label)
{
  label_ = label;
}

bool finalLeaf::isFinalLeaf()
{
  return true;
}
