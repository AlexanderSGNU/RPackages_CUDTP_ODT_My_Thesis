#include "../inst/include/CUDTP.h"

// [[Rcpp::export]]
IntegerVector testMpintVectorConstructor(int d)
{
  return mpintVector(d).ToIntegerVector();
}

// [[Rcpp::export]]
string testBitVector(size_t num,bool clear = false)
{
  Bitvector b(num,clear);
  return b.toString();
}

// [[Rcpp::export]]
string testBitVectorString(string str)
{
  unsigned char* c = new unsigned char[str.size()+1];
  strcpy((char *) c,str.c_str());
  Bitvector b(c,str.size());
  return b.toString();
}

// [[Rcpp::export]]
string testBitVectorHexString(string str)
{
  Bitvector b(str,str.length()/2*8);
  return b.toString();
}

// [[Rcpp::export]]
string testBitVectorHexString2(string str,size_t blength)
{
  Bitvector b(str,blength);
  return b.toString();
}

// [[Rcpp::export]]
string testBitVectorSetClearToggle(string str, size_t index)
{
  unsigned char* c = new unsigned char[str.size()+1];
  strcpy((char *) c,str.c_str());
  Bitvector b(c,str.size());
  string conc = b.toString();
  b.setBit(index);
  conc.append(b.toString());
  b.clearBit(index);
  conc.append(b.toString());
  b.toggleBit(index);
  conc.append(b.toString());
  b.toggleBit(index);
  conc.append(b.toString());
  return conc;
}

// [[Rcpp::export]]
string testBitVectorSet(string str, size_t index)
{
  Bitvector b(str,str.length()/2*8);
  b.setBit(index);
  return b.toString();
}

// [[Rcpp::export]]
string testBitVectorClear(string str, size_t index)
{
  Bitvector b(str,str.length()/2*8);
  b.clearBit(index);
  return b.toString();
}

// [[Rcpp::export]]
string testBitVectorToggle(string str, size_t index)
{
  Bitvector b(str,str.length()/2*8);
  b.toggleBit(index);
  return b.toString();
}

// [[Rcpp::export]]
IntegerVector testMpintVectorToIntegerVector(const IntegerVector& vec)
{
  return(mpintVector(vec).ToIntegerVector());
}

// [[Rcpp::export]]
bool testIsTree(string str, int dimension)
{
  Bitvector b(str,str.length()/2*8);
  return Tree::isTree(b,dimension);
}

// [[Rcpp::export]]
bool testIsTree2(string str, int dimension, size_t blength)
{
  Bitvector b(str,blength);
  return Tree::isTree(b,dimension);
}

// [[Rcpp::export]]
string testMergeTreeConstructor(vector<string> strings,vector<int> dims, vector<size_t> blengths, vector<size_t> indices)
{
  vector<Tree> trees(strings.size());
  for(size_t i = 0; i < strings.size(); i++)
  {
    trees[i] = Tree(strings[i],blengths[i],dims[i]);
  }
  Tree tree(indices,trees);
  return tree.toBitStream();
}

// [[Rcpp::export]]
IntegerVector testGetMinimalCell(const NumericVector& Xi, int d, int depth)
{
  return getMinimalCell(Xi,d,depth).ToIntegerVector();
}

// [[Rcpp::export]]
List testCalculateParentCell(const IntegerVector& lowIndices, int depth)
{
  cubeCell cube = cubeCell(lowIndices,depth);
  return calculateParentCell(cube).toList();
}

// [[Rcpp::export]]
List testCalculateChildFromIndex(const IntegerVector& lowIndices, int depth, size_t index)
{
  cubeCell cube = cubeCell(lowIndices,depth);
  return calculateChildFromIndex(cube,index).toList();
}

// [[Rcpp::export]]
size_t testCalculateChildIndex(const IntegerVector& lowIndices, int depth)
{
  cubeCell cube = cubeCell(lowIndices,depth);
  return calculateChildIndex(cube);
}

// [[Rcpp::export]]
bool testCubeCellCompare(const IntegerVector& lowIndices1,const IntegerVector& lowIndices2, int depth)
{
  return cubeCell(lowIndices1,depth) < cubeCell(lowIndices2,depth);
}

// [[Rcpp::export]]
List testCubeCellSort(const IntegerVector& lowIndices1,
  const IntegerVector& lowIndices2,
  const IntegerVector& lowIndices3,
  int depth)
{
  vector<cubeCell> vec;
  vec.push_back(cubeCell(lowIndices1,depth));
  vec.push_back(cubeCell(lowIndices2,depth));
  vec.push_back(cubeCell(lowIndices3,depth));
  sort(vec);
  return List::create(vec[0].toList(),vec[1].toList(),vec[2].toList());
}

// [[Rcpp::export]]
bool testBitVectorGet(string str, size_t index)
{
  unsigned char* c = new unsigned char[str.size()+1];
  strcpy((char *) c,str.c_str());
  Bitvector b(c,str.size());
  return b.getBit(index);
}

// [[Rcpp::export]]
string testTreeConstructor(string str,size_t blength, int dim)
{
  Tree t(str,blength,dim);
  return t.toString();
}

// [[Rcpp::export]]
string testTree(string str,size_t blength, int dim)
{
  Tree t(str,blength,dim);
  return t.toBitStream();
}

// [[Rcpp::export]]
string testBitStream(string str,size_t blength)
{
  Bitvector b(str,blength);
  return b.toBitStream();
}

