test_that("calculateParentCell",{
  expect_equal(testCalculateParentCell(c(0,1,2,3),2),list(c(0,0,1,1),1))
  expect_equal(testCalculateParentCell(c(0,1,2,3),3),list(c(0,0,1,1),2))
  expect_equal(testCalculateParentCell(c(4,5,6,7),3),list(c(2,2,3,3),2))
  expect_equal(testCalculateParentCell(c(1,3,5,6),5),list(c(0,1,2,3),4))
})
