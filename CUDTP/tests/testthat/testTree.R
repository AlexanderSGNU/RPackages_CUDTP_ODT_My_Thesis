test_that("isTree",{
  # bitvector as string, dimension, number of bits
  expect_true(testIsTree("",0))
  expect_true(testIsTree("",2))
  expect_true(testIsTree2("0100",2,8))
  expect_true(testIsTree2("0200",2,8))
  expect_true(testIsTree2("0400",2,8))
  expect_true(testIsTree2("0800",2,8))
  expect_true(testIsTree2("1300",2,16))
  expect_true(testIsTree2("00",3,8))
  expect_true(testIsTree2("0100",3,16))
  expect_true(testIsTree2("030000",3,24))
  expect_true(testIsTree2("010100",3,24))
  expect_true(testIsTree2("110000",3,24))
  expect_true(testIsTree2("030000",2,12))
  expect_true(testIsTree2("330000",2,20))
  expect_true(testIsTree2("8808",2,16))
  expect_false(testIsTree2("8880",2,16))
  expect_false(testIsTree("50",2))
  expect_false(testIsTree("00000000",2))
  expect_true(testIsTree("0000",4))
  expect_true(testIsTree("00010000",4))
  expect_false(testIsTree("00010001",4))
  expect_false(testIsTree("00030000",4))
})

test_that("TreeConstructor, toString",{
  # bitvector, number of bits, dimension
  expect_equal(testTreeConstructor("00",4,2),"00 2")
  expect_equal(testTreeConstructor("01",8,2),"01 2")
  expect_equal(testTreeConstructor("03",6,1),"03 1")
  expect_equal(testTreeConstructor("c3",6,1),"c3 1")
  expect_equal(testTreeConstructor("f3000000",28,2),"f3 00 00 00 2")
  expect_error(testTreeConstructor("f3",6,1),"no complete tree")
  expect_error(testTreeConstructor("f3",8,1),"no complete tree")

})

test_that("toBitStream",{
  expect_equal(testTree("00",4,2), "0000 2")
  expect_error(testTree("01",5,2),"no complete tree")
  expect_error(testTree("01",6,2),"no complete tree")
  expect_error(testTree("01",7,2),"no complete tree")
  expect_equal(testTree("01",8,2), "10000000 2")
  expect_equal(testTree("010100",24,3), "100000001000000000000000 3")
  expect_equal(testTree("018000",24,3), "100000000000000100000000 3")
})

test_that("merge Tree constructor",{
  # index starting at 0
  expect_equal(testMergeTreeConstructor(c("01","01"),c(2,2),c(8,8),c(0,1)),"11001000100000000000 2")
  expect_equal(testMergeTreeConstructor(c("01","01"),c(2,2),c(8,8),c(0,3)),"10011000100000000000 2")
  expect_equal(testMergeTreeConstructor(c("01","01"),c(2,2),c(8,8),c(2,3)),"00111000100000000000 2")
})
