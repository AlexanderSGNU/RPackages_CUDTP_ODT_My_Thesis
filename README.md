# Easy Setup with Docker

Probably the easiest and fastest way to try the packages, is to use docker.
There is a docker image uploaded [cudtp_odt](https://gitlab.com/AlexanderSGNU/RPackages_CUDTP_ODT_My_Thesis/container_registry)

It uses jupyter-notebook to run the R-code. You can start it with

**docker run -p 8080:8080 registry.gitlab.com/alexandersgnu/rpackages_cudtp_odt_my_thesis/cudtp_odt:latest**

Then copy the link printed in the console and open it with the browser of your choice.  

[createEvaluation.ipynb](https://gitlab.com/AlexanderSGNU/RPackages_CUDTP_ODT_My_Thesis/blob/master/comparison_public/createEvaluation.ipynb) is the file you want to run with jupyter.

# Compiling dockerfile

There is also a dockerfile and the necessary file for selfcompilation. [Dockerdir](https://gitlab.com/AlexanderSGNU/RPackages_CUDTP_ODT_My_Thesis/tree/master/Docker)

# No Docker

Have a look at the dockerfile and [old_packagelist_install.R](https://gitlab.com/AlexanderSGNU/RPackages_CUDTP_ODT_My_Thesis/blob/master/Docker/old_packagelist_install.R).
They tell you which libraries and R packages you need to get it running. 

# Link to my Diploma Thesis

It is written in German. [Diploma Thesis](https://nbn-resolving.org/urn:nbn:de:bsz:15-qucosa2-167566)