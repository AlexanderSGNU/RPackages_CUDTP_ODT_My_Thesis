require(class)

  predicted[[method_index]] <- knn(xy_train[[1]],xy_test[[1]],factor(xy_train[[2]]),k = k)

confMatrix[[method_index]] <- caret::confusionMatrix(predicted[[method_index]],factor(xy_test[[2]]))