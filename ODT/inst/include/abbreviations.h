#ifndef __ABBREVIATIONS__
#define __ABBREVIATIONS__

#include "./include_odt.h"
#include "./cuboidCellkmax.h"
#include "./cuboidExt.h"
#include "./coordinateHash.h"
#include "./size_tVectorkmax.h"
#include "./size_tVector.h"
#include "./cuboidPartition.h"

#define cubpair make_pair<cuboidCellkmax,cuboidExt>
typedef unordered_map<size_tVectorkmax,size_tVector,coordinateHashSingle> sizetmap;
#define sizetpair make_pair<size_tVectorkmax,size_tVector>
typedef unordered_map<cuboidCellkmax,cuboidPartition, coordinateHash> partitionmap;
#define partitionpair make_pair<cuboidCellkmax,cuboidPartition>

#endif
