#ifndef __COORDINATE_HASH__
#define __COORDINATE_HASH__

class coordinateHash
{
  public:
    size_t operator()(const cuboidCellkmax& cellk) const;
};

class coordinateHashSingle
{
  public:
    size_t operator()(const size_tVectorkmax& veck) const;
};

#endif
