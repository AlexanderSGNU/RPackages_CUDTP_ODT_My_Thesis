#ifndef __TEST_HELPER_FUNCTIONS_H_INCLUDED__
#define __TEST_HELPER_FUNCTIONS_H_INCLUDED__

#include "./cuboidCell.h"
#include "./cuboidCellkmax.h"
#include "./cuboidExt.h"
#include "./size_tVectorkmax.h"
#include "./size_tVector.h"
#include "./cuboidPartition.h"
#include "./doubleVector.h"
#include "./nbyIndexTuple.h"
#include "./abbreviations.h"
#include "./include_odt.h"
#include "./ODT_templates.hxx"

class fMisclass
{
  private:
    const cuboidPartition* part;
    size_t d, kmax;
  public:
    fMisclass(const cuboidPartition* p,size_t d, size_t kmax);
    int operator()(const doubleVector& x);
};

size_tVector getMinimalCell(const doubleVector& Xi, size_t d, size_t kmax);
bool hasSibling(const cuboidCell& cub, size_t d, size_t kmax);
cuboidCell getSibling(cuboidCell cub,size_t d,size_t kmax);
cuboidCell getParent(cuboidCell cub,size_t d1,size_t kmax);
size_t getNby(nbyIndexTuple& l);
double costfunction(const cuboidPartition &part);
List getOptimalTree (int n,int d,int kmax, int S, double gamma, NumericMatrix Xvector,const IntegerVector& Yvector);
size_t fbxPlugin(cuboidCellExt cub);

#endif
