#ifndef __CUBOID_PARTITION__
#define __CUBOID_PARTITION__

#include "./include_odt.h"
#include "./cuboidCell.h"
#include "./size_tVector.h"
#include "./cuboidCellExt.h"
#include "./cuboidCellkmax.h"
#include "./coordinateHash.h"
#include "./cuboidExt.h"

typedef unordered_map<cuboidCellkmax,cuboidExt,coordinateHash> cubmap;

class cuboidPartition
{
  public:
    cuboidCell cell;
    vector<cubmap::iterator > vec_ptr;
    cuboidPartition();
    cuboidPartition(const cuboidCell& cub);
    cuboidPartition(const cuboidCell& cub,  const vector<cubmap::iterator >& vec_ptr);
    cuboidPartition(const cuboidCell& cub,  cubmap::iterator it);
    cuboidPartition(const size_tVector& lInd, const size_tVector& hInd);
    cuboidPartition(const cuboidPartition& p1, const cuboidPartition& p2);
    cuboidPartition(const cuboidPartition& cubP);
    cuboidPartition& operator=(const cuboidPartition& cubP);
    vector<cubmap::iterator > concatenateVec( const vector<cubmap::iterator >& ptr1, const vector<cubmap::iterator >& ptr2);
    size_t getSize() const;
    static cuboidCell getCuboidCell(const cuboidPartition& p);
    cuboidCell getCuboidCell();
    cuboidCellExt getCuboidCellExt(size_t index) const;
    List toList() const;
};

#endif
