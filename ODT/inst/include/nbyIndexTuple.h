#ifndef __NBY_INDEX_TUPLE__
#define __NBY_INDEX_TUPLE__

#include "./include_odt.h"
#include "./size_tVector.h"

class nbyIndexTuple
{
public:
  size_tVector nby;
  size_t y;
  nbyIndexTuple();
  nbyIndexTuple(const size_tVector& nby, size_t y);
  nbyIndexTuple(const NumericVector& nbyVec, size_t y);
};

#endif
