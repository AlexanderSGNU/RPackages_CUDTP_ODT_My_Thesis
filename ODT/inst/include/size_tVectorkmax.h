#ifndef __SIZET_VECTOR_KMAX__
#define __SIZET_VECTOR_KMAX__

#include "./include_odt.h"
#include "./size_tVector.h"

class size_tVectorkmax
{
  public:
    size_tVector vec;
    size_t kmax;
    size_tVectorkmax(size_tVector v, size_t k);
    bool operator== (const size_tVectorkmax& vect) const;
};

#endif
