#ifndef __ODT_TEMPLATES__
#define __ODT_TEMPLATES__

template <class ArgType, class ReturnValue>
ArgType ArgMin(ReturnValue (*f)(ArgType),ArgType arg1,ArgType arg2)
{
  if(f(arg1)>f(arg2))
    return arg2;
  return arg1;
}

/*
# f is classifier, (x,y) is observed data pair
*/

template <class Ty, class Tx>
size_t MisclassificationLoss(Ty (*f)(Tx), Tx x, Ty y)
{
  if(f(x) == y)
    return 0;
  return 1;
}


template <class Tx, class Ty, class Tz>
Tx ArgMax(Ty (*f)(Tx),  Tz& vec)
{
  if(vec.size() == 1)
    return vec[0];
  Tx argmax = vec[0];
  for(size_t index = 1; index < vec.size();index++)
  {
    if(f(argmax) < f(vec[index]))
      argmax = vec[index];
  }
  return argmax;
}

#endif
