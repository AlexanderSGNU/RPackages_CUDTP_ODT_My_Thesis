#ifndef __DOUBLE_VECTOR__
#define __DOUBLE_VECTOR__

#include "./include_odt.h"

class doubleVector
{
public:
  vector<double> v;
  doubleVector();
  doubleVector(const NumericMatrix nm, size_t index);
  doubleVector(const NumericVector& vec);
  void initDoubleVector(const NumericVector& vec);
  size_t size() const;
  double operator[](size_t index) const;
};

#endif
