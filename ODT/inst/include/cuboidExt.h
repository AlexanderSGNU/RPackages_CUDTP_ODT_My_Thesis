#ifndef __CUBOID_EXT__
#define __CUBOID_EXT__

#include "./include_odt.h"
#include "./size_tVector.h"

class cuboidExt
{
  public:
    size_tVector nbyValues;
    double cost;
    cuboidExt();
    cuboidExt(const size_tVector& nby, double c);
    cuboidExt(const cuboidExt& cExt);
};

#endif
