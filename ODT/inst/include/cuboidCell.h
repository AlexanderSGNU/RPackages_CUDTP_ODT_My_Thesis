#ifndef __CUBOID_CELL__
#define __CUBOID_CELL__

#include "./include_odt.h"
#include "./size_tVector.h"

class cuboidCell
{
  public:
    size_tVector lowIndices, highIndices;
    cuboidCell();
    cuboidCell(const size_tVector& lInd, const size_tVector& hInd);
    cuboidCell(const cuboidCell& cC);
    cuboidCell& operator= (const cuboidCell& cC);
    bool operator== (const cuboidCell& cC) const;
    size_t getDimension() const;
    size_t getCellVolume() const;
    List toList() const;
    static List toList(const cuboidCell& cub);
    cuboidCell concatenateCells(const cuboidCell& c2) const;
    static size_t productVectorElements(const size_tVector& vec);
};

#endif
