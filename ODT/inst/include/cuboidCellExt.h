#ifndef __CUBOID_CELL_EXT__
#define __CUBOID_CELL_EXT__

#include "./include_odt.h"
#include "./cuboidCell.h"
#include "./cuboidExt.h"
#include "./cuboidCellkmax.h"
#include "./size_tVector.h"

class cuboidCellExt
{
  public:
    cuboidCell cell;
    cuboidExt ext;
    cuboidCellExt();
    cuboidCellExt(const size_tVector& lInd, const size_tVector& hInd, const size_tVector& nby, double cost);
    cuboidCellExt(const cuboidCell& cub, const size_tVector& nby, double cost);
    cuboidCellExt(const cuboidCell& cub, const cuboidExt& ext);
    cuboidCellExt(const cuboidCellkmax& cub, const cuboidExt& ext);
    size_t getDimension() const;
    size_t getCellVolume() const;
    List toList() const;
    static List toList(const cuboidCellExt& cub);
};

#endif
