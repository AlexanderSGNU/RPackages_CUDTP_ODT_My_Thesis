#ifndef __O_D_P__
#define __O_D_P__

#include "./ODT.h"

class odp
{
  public:
    List part;
    odp(int n,int d,int kmax, int S, double gamma, NumericMatrix Xvector, IntegerVector Yvector);
    List toList();
};

#endif
