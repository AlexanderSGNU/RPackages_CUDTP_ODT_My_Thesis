#ifndef __CUBOID_CELL_KMAX__
#define __CUBOID_CELL_KMAX__

#include "./include_odt.h"
#include "./cuboidCell.h"
#include "./size_tVectorkmax.h"

class cuboidCellkmax
{
  public:
    cuboidCell cell;
    size_t kmax;
    cuboidCellkmax(const cuboidCell& c, size_t k);
    cuboidCellkmax(const size_tVectorkmax& vec);
    bool operator== (const cuboidCellkmax& cC) const;
};

#endif
