#ifndef __SIZET_VECTOR__
#define __SIZET_VECTOR__

#include "./include_odt.h"

class size_tVector
{
  public:
    vector<size_t> v;
    size_tVector();
    size_tVector(vector<size_t> vec);
    size_tVector(const size_tVector& vec);
    size_tVector(size_t numberOfElements);
    size_tVector(const IntegerVector& vec);
    size_tVector& operator=(const size_tVector& vec);
    size_tVector operator +(const size_tVector& v2) const;
    size_tVector operator +(size_t add) const;
    size_tVector operator -(size_t sub) const;
    size_tVector operator -(const size_tVector& v2) const;
    size_tVector& operator += (const size_tVector& v2);
    size_tVector& operator -= (const size_tVector& v2);
    bool operator== (const size_tVector& vec) const;
    bool operator <(const double& number) const;
    bool operator <(const size_tVector& sVec) const;
    bool operator <=(const size_tVector& vec) const;
    size_t size()  const;
    size_t& operator[](size_t index);
    const size_t operator()(size_t index) const;
    IntegerVector ToIntegerVector() const;
};

#endif
