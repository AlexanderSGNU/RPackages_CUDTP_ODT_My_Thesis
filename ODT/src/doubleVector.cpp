#include "../inst/include/doubleVector.h"

doubleVector::doubleVector(){}

// get index .th row of nm
doubleVector::doubleVector(const Rcpp::NumericMatrix nm, size_t index)
{
  int size = nm.ncol();
  Rcpp::NumericVector nv(size);
  for(int coln = 0; coln < size;coln++)
  {
    nv(coln) = nm(index,coln);
  }
  initDoubleVector(nv);
}

doubleVector::doubleVector(const Rcpp::NumericVector& vec)
{
  initDoubleVector(vec);
}

void doubleVector::initDoubleVector(const Rcpp::NumericVector& vec)
{
  size_t size = vec.size();
  v.resize(size);
  for(size_t index = 0; index < size;index++)
  {
    v[index] = vec[index];
  }
}

size_t doubleVector::size() const
{
  return v.size();
}

double doubleVector::operator[](size_t index) const
{
  return v[index];
}
