#include "../inst/include/size_tVector.h"

size_tVector::size_tVector(){}
size_tVector::size_tVector(vector<size_t> vec): v(vec){}
size_tVector::size_tVector(const size_tVector& vec): v(vec.v){}
size_tVector::size_tVector(size_t numberOfElements): v(vector<size_t>(numberOfElements)){}

size_tVector::size_tVector(const IntegerVector& vec)
{
  size_t size = vec.size();
  v.resize(size);
  for(size_t index = 0; index < size;index++)
  {
    v[index] = vec[index];
  }
}

size_tVector& size_tVector::operator=(const size_tVector& vec)
{
  v = vector<size_t>(vec.v);
  return *this;
}

size_tVector size_tVector::operator +(const size_tVector& v2) const
{
  size_tVector vec = size_tVector(*this);
  vec += v2;
  return vec;
}

size_tVector size_tVector::operator +(size_t add) const
{
  size_tVector vec;
  size_t size = v.size();
  vec.v.resize(size);
  for(size_t index = 0; index < size; index++)
  {
    vec[index]=v[index] + add;
  }
  return vec;
}

size_tVector size_tVector::operator -(size_t sub) const
{
  size_tVector vec;
  size_t size = v.size();
  vec.v.resize(size);
  for(size_t index = 0; index < size; index++)
  {
    vec[index]=v[index]-sub;
  }
  return vec;
}

size_tVector size_tVector::operator -(const size_tVector& v2) const
{
  size_tVector vec = *this;
  vec -= v2;
  return vec;
}

size_tVector& size_tVector::operator += (const size_tVector& v2)
{
  int size = v2.size();
  for(int index = 0; index < size;index++)
  {
    v[index] += v2(index);
  }
  return *this;
}

size_tVector& size_tVector::operator -= (const size_tVector& v2)
{
  int size = v2.size();
  for(int index = 0; index < size;index++)
  {
    v[index] -= v2(index);
  }
  return *this;
}

bool size_tVector::operator== (const size_tVector& vec) const
{
  int size = this->size();
  for(int index = 0; index < size; index++)
  {
    if(v[index] != vec(index))
      return false;
  }
  return true;
}

// returns true if one element is < number

bool size_tVector::operator <(const double& number) const
{
  for(size_t element:v)
  {
    if(element < number)
      return true;
    if(element > number)
      return false;
  }
  return false;
}

bool size_tVector::operator <(const size_tVector& sVec) const
{
  for(size_t index = 0; index < v.size();index++)
  {
    if(v[index] < sVec(index))
      return true;
    if(v[index] > sVec.v[index])
      return false;
  }
  return false;
}

// returns true if elementwise comparison returns true for every element

bool size_tVector::operator <=(const size_tVector& vec) const
{
  size_t size = v.size();
  for(size_t index = 0; index < size;index++)
  {
    if(v[index] > vec(index))
      return false;
  }
  return true;
}

size_t size_tVector::size()  const
{
  return v.size();
}

size_t& size_tVector::operator[](size_t index)
{
  return v[index];
}

const size_t size_tVector::operator()(size_t index) const
{
  return v[index];
}

Rcpp::IntegerVector size_tVector::ToIntegerVector() const
{
  return Rcpp::IntegerVector(v.begin(),v.end());
}
