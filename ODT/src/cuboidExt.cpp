#include "../inst/include/cuboidExt.h"

cuboidExt::cuboidExt(){}

cuboidExt::cuboidExt(const size_tVector& nby, double c): nbyValues(size_tVector(nby)), cost(c){}

cuboidExt::cuboidExt(const cuboidExt& cExt): nbyValues(size_tVector(cExt.nbyValues)), cost(cExt.cost){}
