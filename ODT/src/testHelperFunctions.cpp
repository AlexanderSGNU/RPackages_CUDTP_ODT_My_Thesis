#include "../inst/include/ODT.h"

// [[Rcpp::export]]
List To_size_tVectorAndBack(IntegerVector vec)
{
  return List::create(size_tVector(vec).ToIntegerVector());
}

List testCub(size_tVector lInd, size_tVector hInd)
{
 cuboidCell cub = cuboidCell(lInd,hInd);
 return cub.toList();
}

// [[Rcpp::export]]
List testCub(IntegerVector lInd, IntegerVector hInd)
{
  return testCub(size_tVector(lInd), size_tVector(hInd));
}

List testCubExt(size_tVector lInd, size_tVector hInd, size_tVector nby, double cost)
{
 cuboidCellExt cub = cuboidCellExt(lInd,hInd,nby,cost);
 return cub.toList();
}

// [[Rcpp::export]]
List testCubExt(IntegerVector lInd, IntegerVector hInd, IntegerVector nby, double cost)
{
  return testCubExt(size_tVector(lInd), size_tVector(hInd), size_tVector(nby), cost);
}

List testGetSibling(size_tVector lInd, size_tVector hInd)
{
  cuboidCell cub = cuboidCell(lInd,hInd);
  size_t kmax = 2, d = 2;
  return getSibling(cub,d,kmax).toList();
}

// [[Rcpp::export]]
List testGetSibling(IntegerVector lInd, IntegerVector hInd)
{
  return testGetSibling(size_tVector(lInd), size_tVector(hInd));
}

bool testHasSibling(size_tVector lInd, size_tVector hInd)
{
  cuboidCell cub = cuboidCell(lInd,hInd);
  size_t kmax = 2, d = 2;
  return hasSibling(cub,d,kmax);
}

// [[Rcpp::export]]
bool testHasSibling(IntegerVector lInd, IntegerVector hInd)
{
  return testHasSibling(size_tVector(lInd), size_tVector(hInd));
}

List testGetParent(size_tVector lInd, size_tVector hInd)
{
  cuboidCell cub = cuboidCell(lInd,hInd);
  size_t kmax = 2, d = 2;
  return getParent(cub,d,kmax).toList();
}

// [[Rcpp::export]]
List testGetParent(IntegerVector lInd, IntegerVector hInd)
{
  return testGetParent(size_tVector(lInd), size_tVector(hInd));
}

double fun(List l)
{
  return l(0)*(double)l(1);
}

//' @title Test ArgMin
//' @param x = argument1
//' @param y = argument2
//' @return ArgMin
// [[Rcpp::export]]
List test(double x,double y)
{
  List l1 = List::create(x,x);
  List l2 = List::create(y,y);
  return ArgMin(fun,l1,l2);
}

// [[Rcpp::export]]
List testGetMinimalCell(NumericVector vec, int d, int kmax)
{
  return List::create(getMinimalCell(doubleVector(vec),d,kmax).ToIntegerVector());
}

//' @title testGetNby
//' @description test getNby
//' @param nbyVec = nby vector
//' @param y = nby index to extract
//' @return nby value at index ys
// [[Rcpp::export]]
int testGetNby(const Rcpp::NumericVector nbyVec, size_t y)
{
  nbyIndexTuple nIT = nbyIndexTuple(nbyVec,y);
  return getNby(nIT);
}
