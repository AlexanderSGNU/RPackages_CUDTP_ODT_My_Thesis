#include "../inst/include/size_tVectorkmax.h"

size_tVectorkmax::size_tVectorkmax(size_tVector v, size_t k): vec(size_tVector(v)), kmax(k){}

bool size_tVectorkmax::operator== (const size_tVectorkmax& vect) const
{
  return vec==vect.vec;
}
