#include "../inst/include/odp.h"

odp::odp(int n,int d,int kmax, int S, double gamma, NumericMatrix Xvector, IntegerVector Yvector)
{
  part = getOptimalTree(n, d, kmax, S, gamma, Xvector, Yvector);
}

List odp::toList()
{
  return part;
}

// exporting class odp to R

RCPP_MODULE(mod_odp){
  class_<odp>("odp")

  .constructor<int, int, int, int, double, NumericMatrix, IntegerVector>()

  .method("toList", &odp::toList)
  ;
}
