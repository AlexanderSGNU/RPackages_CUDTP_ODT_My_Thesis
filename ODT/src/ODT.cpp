#include "../inst/include/ODT.h"

// need number of bits in a byte -> CHAR_BIT = 8 ?
// need number of bytes of size_t = 8 ?

// [[Rcpp::plugins("cpp11")]]

// size_t could be too small, that happens if 2*dim*kmax > number of bytes of size_t * number of bits of 1 byte

size_t coordinateHash::operator()(const cuboidCellkmax& cellk) const
{
  size_t ret = 0, kmax = cellk.kmax, dim = cellk.cell.getDimension();
  for(size_t index = 0; index < dim; index++)
  {
    ret = ret + (cellk.cell.lowIndices(index) << (kmax*index));
  }
  for(size_t index = 0; index < dim; index++)
  {
    ret = ret + (cellk.cell.highIndices(index) << (kmax*(dim+index)));
  }
  return ret;
}

// size_t could be too small, that happens if dim*kmax > number of bytes of size_t * number of bits of 1 byte

size_t coordinateHashSingle::operator()(const size_tVectorkmax& veck) const
{
  size_t ret = 0, kmax = veck.kmax, dim = veck.vec.size();
  for(size_t index = 0; index < dim; index++)
  {
    ret = ret + (veck.vec(index) << (kmax*index));
  }
  return ret;
}

////' @title getMinimalCell
////' @description
////' bi is vector of indices with dimension d
////' if Xi[index] == 1 special handling
////' @param Xi : vector of dimension d
////' @param d : dimension
////' @param kmax : maximal splits in direction d
////' @return cell bi which contains Xi
size_tVector getMinimalCell(const doubleVector& Xi, size_t d, size_t kmax)
{
  size_tVector indexVec;
  size_t size = d;
  indexVec.v.resize(size);
  for(size_t index = 0; index < size;index++)
  {
    if(Xi[index] == 1.0)
    {
      indexVec[index] = pow(2,kmax)-1;
    }
    else
    {
      indexVec[index] = floor(Xi[index]*pow(2,kmax));
    }
  }
  return indexVec;
}

/*
# checks if size of cell at dimension d still fits, if it is doubled
# bounded by 2^kmax
# cell = (x_1,x_2) in N^d x N^d
*/

bool hasSibling(const cuboidCell& cub, size_t d, size_t kmax)
{
  if(cub.lowIndices.size() < d || cub.highIndices.size() < d)
    throw(Rcpp::exception("dimension of cell is wrong"));
  if((cub.highIndices(d-1)-cub.lowIndices(d-1)+1)*2 > pow(2,kmax))
    return false;
  return true;
}

// no const reference, because copy will be changed and returned

cuboidCell getSibling(cuboidCell cub,size_t d,size_t kmax)
{
  if(cub.lowIndices.size() < d || cub.highIndices.size() < d)
    throw(Rcpp::exception("dimension of cell is wrong"));
  size_t delta = cub.highIndices(d-1)-cub.lowIndices(d-1)+1,
    remainder = (cub.lowIndices(d-1) / delta ) % 2;
  if(remainder == 0)
  {
    cub.lowIndices[d-1] += delta;
    cub.highIndices[d-1] += delta;
  }
  else
  {
    cub.lowIndices[d-1] -= delta;
    cub.highIndices[d-1] -= delta;
  }
  return cub;
}

// no const reference, because copy will be changed and returned

cuboidCell getParent(cuboidCell cub,size_t d,size_t kmax)
{
  if(cub.lowIndices.size() < d || cub.highIndices.size() < d)
    throw(Rcpp::exception("dimension of cell is wrong"));
  size_t delta = cub.highIndices(d-1)-cub.lowIndices(d-1)+1,
    remainder = (cub.lowIndices(d-1) / delta ) %2;
  if(remainder == 0)
  {
    cub.highIndices[d-1] += delta;
  }
  else
  {
    cub.lowIndices[d-1] -= delta;
  }
  return cub;
}

/*
  l = (size_tVector nby,size_t &y)
*/

nbyIndexTuple::nbyIndexTuple(){}

nbyIndexTuple::nbyIndexTuple(const size_tVector& nby, size_t y):nby(nby), y(y){}

nbyIndexTuple::nbyIndexTuple(const Rcpp::NumericVector& nbyVec, size_t y):y(y)
{
  size_t size = nbyVec.size();
  nby.v.resize(size);
  for(size_t index = 0; index < size;index++)
  {
    nby[index] = nbyVec(index);
  }
}

size_t getNby(nbyIndexTuple& l)
{
  return l.nby(l.y-1);
}

/*
# majority vote for classification
# cell = {((vector1),(vector2)),vector(1:S) of Nby,cost}
# vector1 = lowest indices, vector2 = highest indices
*/

size_t fbxPlugin(cuboidCellExt cub)
{
  size_t size = cub.ext.nbyValues.size();
  vector<nbyIndexTuple> NbyArgs;
  NbyArgs.reserve(size);
  for(size_t y = 0; y < size;y++)
  {
    NbyArgs.push_back(nbyIndexTuple(cub.ext.nbyValues,y+1));
  }
  return ArgMax(getNby,NbyArgs).y;
}

fMisclass::fMisclass(const cuboidPartition* p,size_t d, size_t kmax):part(p),d(d),kmax(kmax){}
int fMisclass::operator()(const doubleVector& x)
{
  const size_tVector minCell = getMinimalCell(x,d,kmax);
  for(size_t index = 0; index < part->getSize();index++)
  {
    //      # if x is in cell (between lower left and upper right)
    if(part->cell.lowIndices <= minCell && minCell <= part->cell.highIndices)
    {
      return fbxPlugin(part->getCuboidCellExt(index));
    }
  }
  throw(Rcpp::exception("cell = NULL"));
  return 0;
}

/*
# cuboidPartition = {(cell,nbys,cost(cell)), other cells}
*/

double costfunction(const cuboidPartition &part)
{
  double sum = 0;
//  # use additivity property and stored values
// optimization with vectormultp ???: store cost values in vector, use scalarmultiplication, add to sum
  for(auto it = part.vec_ptr.begin() ; it != part.vec_ptr.end(); ++it)
    sum += (*it)->second.cost;
  return sum;
}

////' @title Get OptimalDecisionTree
////' @description returns the partition of the ODT
////' @return List of list: {(cell.lowIndices,cell.highIndices,Nby,cost), ...}
////' @param n = number of Datapairs (X,Y)
////' @param d = dimension of X
////' @param kmax = number of maximal splits in one direction
////' @param S = number of different classes
////' @param gamma = penalty for every additional cell in the resulting partition
////' @param Xvector = X, observed Data
////' @param Yvector = Y, class
List getOptimalTree (int n,int d,int kmax, int S, double gamma, Rcpp::NumericMatrix Xvector,const Rcpp::IntegerVector& Yvector)
{
  partitionmap dictionary[2];
  vector<size_tVector> minCells;
  minCells.resize(n);
  list<cubmap> trivialCells;
  sizetmap cellContains(n);

//  # help functions

//  # calculate cost for trivial cuboidPartition
  auto costtrivial = [&cellContains,&Xvector,&Yvector,&gamma,&n](const cuboidPartition &part, int d, int kmax) -> double
  {
    auto hatfB = fMisclass(&part,d,kmax);
    auto Loss = [&hatfB](const doubleVector& x,int y)
    {
      if(hatfB(x) == y)
        return 0;
      return 1;
    };

    double sum = 0;
    const size_tVector * indexvector = &cellContains.at(size_tVectorkmax(part.cell.lowIndices,kmax));
    if((*indexvector).size() > 0)
    {
      for(auto it= indexvector->v.begin(); it != indexvector->v.end(); it++)
      {
        // ... + lossfunction
        sum = sum + Loss(doubleVector(Xvector,*it),Yvector(*it));
      }
    }
    return sum/n + gamma;
  };

//  # create {u} cuboidPartition with area = area of p1 united area of p2
  auto createPartitionFromParts = [&](const cuboidPartition &p1,const cuboidPartition &p2)
  {
    size_t dim = p1.cell.getDimension();
    size_tVector NbyVector(S);
//    # for all parts of p1, sum nbyValues
    for( auto it = p1.vec_ptr.begin(); it != p1.vec_ptr.end();it++)
    {
      NbyVector += (*it)->second.nbyValues;
    }
//    # for all parts of p2, sum nbyValues
    for( auto it = p2.vec_ptr.begin(); it != p2.vec_ptr.end();it++)
    {
      NbyVector += (*it)->second.nbyValues;
    }

    auto fPlugin = [&](const doubleVector& x)
    {
      vector<nbyIndexTuple> NbyArgs;
      NbyArgs.resize(S);
      for(int y = 1; y <=S;y++)
      {
        NbyArgs[y-1] = nbyIndexTuple(NbyVector,y);
      }
      return ArgMax(getNby,NbyArgs).y;
    };

    auto lossfunction = [&](const doubleVector& x,size_t y)
    {
      if(fPlugin(x) == y)
        return 0;
      return 1;
    };

    cuboidCell cell = p1.cell.concatenateCells(p2.cell);
    size_tVector differences = cell.highIndices-cell.lowIndices+1;
    size_tVector coordindex(dim);
    size_t end = cuboidCell::productVectorElements(differences);
    double cost = 0;
    for(size_t index = 0; index < end; index++)
    {
      size_t value = index;
      for(size_t jindex = 0; jindex < dim; jindex++)
      {
        coordindex[jindex] = cell.lowIndices(jindex) + value % differences(jindex);
        value = value / differences(jindex);
      }
      auto iter = cellContains.find(size_tVectorkmax(coordindex,kmax));
      if(iter != cellContains.end())
      {
        size_t size = iter->second.size();
        if(size > 0)
        {
          for(size_t k = 0; k < size; k++)
          {
            cost = cost + lossfunction(doubleVector(Xvector,iter->second(k)),Yvector(iter->second(k)));
          }
        }
      }
    }
    cost = cost/n + gamma;
    return cuboidCellExt(cell,NbyVector,cost);
  };

//  # save indices of observations, which are in specific cell
  for(int index = 0; index < n;index++)
  {
    minCells.at(index) = getMinimalCell(doubleVector(Xvector(Range(0,n-1),Range(0,d-1)),index),d,kmax);
    auto iter = cellContains.find(size_tVectorkmax(minCells[index],kmax));
    if(iter == cellContains.end())
    {
      cellContains.insert( sizetpair(
        size_tVectorkmax(minCells[index],kmax),
        size_tVector(vector<size_t>(1,index)) ));
    }
    else
    {
      iter->second.v.push_back(index);
    }
  }
  auto trivOldIt = trivialCells.insert(trivialCells.begin(),cubmap());
  dictionary[0] = partitionmap();
  auto dictionaryIt = &(dictionary[0]);
  for(auto iter = cellContains.begin(); iter != cellContains.end(); iter++)
  {
    size_tVector newNby(S);
    for(auto iterat = iter->second.v.begin(); iterat != iter->second.v.end();iterat++)
    {
      // Yvalues in 1:S, [value] must be in 0:(S-1)
      newNby[Yvector(*iterat)-1]++;
    }
    auto tcpairIterator = trivOldIt->insert(trivOldIt->begin(),cubpair(
      cuboidCellkmax(iter->first),
      cuboidExt(newNby,0) ));
    auto dicpairIterator = dictionaryIt->insert(dictionaryIt->begin(),partitionpair(
      cuboidCellkmax(iter->first),
      cuboidPartition(cuboidCell(iter->first.vec,iter->first.vec),tcpairIterator)));
    tcpairIterator->second.cost = costtrivial(dicpairIterator->second,d,kmax);
  }
  dictionary[1] = partitionmap();
  auto dicNewIt = &(dictionary[1]);
  auto dicOldIt = &(dictionary[0]);
  for(int D = 2; D <= d*kmax+1;D++)
  {
    auto trivNewIt = trivialCells.insert(trivialCells.begin(),cubmap());
    trivOldIt = ++trivialCells.begin();
    for(auto pairIterator = dicOldIt->begin(); pairIterator != dicOldIt->end(); pairIterator++)
    {
      for(int k = 1; k <= d;k++)
      {
        if(hasSibling(pairIterator->first.cell,k,kmax))
        {
          cuboidCell bSibling = getSibling(pairIterator->first.cell,k,kmax);
          cuboidPartition TbSibling;
          auto bSiblingIt = dicOldIt->find(cuboidCellkmax(bSibling,kmax));
          if(dicOldIt->end()==bSiblingIt)
          {
            auto trivPairIt = trivOldIt->find(cuboidCellkmax(bSibling,kmax));
            if(trivPairIt == trivOldIt->end())
            {
              (*trivOldIt)[cuboidCellkmax(bSibling,kmax)] = cuboidExt(size_tVector(S),gamma);
              trivPairIt = trivOldIt->find(cuboidCellkmax(bSibling,kmax));
            }
            TbSibling = cuboidPartition(bSibling,trivPairIt);
          }
          else
          {
            TbSibling = bSiblingIt->second;
          }
          cuboidCell bParent = getParent(pairIterator->first.cell,k,kmax);
          cuboidCellkmax bParentkmax = cuboidCellkmax(bParent,kmax);
          auto pairIt = dicNewIt->find(bParentkmax);
          if(pairIt == dicNewIt->end())
          {
            auto trivPairIt = trivNewIt->find(bParentkmax);
            if(trivPairIt == trivNewIt->end())
            {
              cuboidCellExt trivPartition;
              if(pairIterator->second.cell.lowIndices < TbSibling.cell.lowIndices)
                trivPartition = createPartitionFromParts(pairIterator->second,TbSibling);
              else
                trivPartition = createPartitionFromParts(TbSibling,pairIterator->second);
              trivPairIt = trivNewIt->insert(trivNewIt->begin(),make_pair(bParentkmax,trivPartition.ext));
            }
            cuboidPartition p1 = cuboidPartition(bParent,trivPairIt),p2;
//            # order cuboidPartition with cuboidPartition with lowest indice values at first and higher values last
            if(pairIterator->second.cell.lowIndices < TbSibling.cell.lowIndices)
            {
              p2 = cuboidPartition(pairIterator->second,TbSibling);
            }
            else
            {
              p2 = cuboidPartition(TbSibling,pairIterator->second);
            }
            dicNewIt->insert(dicNewIt->begin(),make_pair(
              bParentkmax,
              ArgMin<const cuboidPartition&,double>(costfunction,p1,p2)));
          }
          else
          {
//            # order cuboidPartition with cuboidPartition with lowest indice values at first and higher values last
            if(pairIterator->second.cell.lowIndices < TbSibling.cell.lowIndices)
            {
              cuboidPartition p2 = cuboidPartition(pairIterator->second,TbSibling);
              pairIt->second = ArgMin<const cuboidPartition&,double>(costfunction,pairIt->second,p2);
            }
            else
            {
              cuboidPartition p2 = cuboidPartition(TbSibling,pairIterator->second);
              pairIt->second = ArgMin<const cuboidPartition&,double>(costfunction,pairIt->second,p2);
            }
          }
        }
      }
    }
    dicOldIt->clear();
    auto dummyIt = dicNewIt;
    dicNewIt = dicOldIt;
    dicOldIt = dummyIt;
  }
  cuboidPartition finalPart = dicOldIt->begin()->second;
  return finalPart.toList();
}
