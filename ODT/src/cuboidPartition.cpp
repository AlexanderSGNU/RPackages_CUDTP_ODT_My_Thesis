#include "../inst/include/cuboidPartition.h"

/*
 cuboidPartition = cuboidCell, vector of pointers to cuboidCellExt
*/

cuboidPartition::cuboidPartition(){}

cuboidPartition::cuboidPartition(const cuboidCell& cub):
cell(cub.lowIndices,cub.highIndices){}

cuboidPartition::cuboidPartition(const cuboidCell& cub,  const vector<cubmap::iterator >& vec_ptr):
cell(cub.lowIndices,cub.highIndices),
vec_ptr( vector<cubmap::iterator >(vec_ptr)){}

cuboidPartition::cuboidPartition(const cuboidCell& cub,  cubmap::iterator it):
cell(cub.lowIndices,cub.highIndices),
vec_ptr( vector<cubmap::iterator >(1,it)){}

cuboidPartition::cuboidPartition(const size_tVector& lInd, const size_tVector& hInd):
cell(lInd,hInd){}

cuboidPartition::cuboidPartition(const cuboidPartition& cubP):
cell(cubP.cell),
vec_ptr(vector<cubmap::iterator >(cubP.vec_ptr)){}

cuboidPartition& cuboidPartition::operator=(const cuboidPartition& cubP)
{
  cell = cubP.cell;
  vec_ptr = vector<cubmap::iterator >(cubP.vec_ptr);
  return *this;
}

cuboidPartition::cuboidPartition(const cuboidPartition& p1, const cuboidPartition& p2):
cell(p1.cell.concatenateCells(p2.cell)),
vec_ptr(concatenateVec(p1.vec_ptr,p2.vec_ptr)){}

vector<cubmap::iterator > cuboidPartition::concatenateVec( const vector<cubmap::iterator >& ptr1, const vector<cubmap::iterator >& ptr2)
{
  vector<cubmap::iterator > concat;
  concat.reserve(ptr1.size()+ptr2.size());
  concat.insert(concat.end(),ptr1.begin(), ptr1.end());
  concat.insert(concat.end(),ptr2.begin(), ptr2.end());
  return concat;
}

size_t cuboidPartition::getSize() const
{
  return vec_ptr.size();
}

cuboidCell cuboidPartition::getCuboidCell(const cuboidPartition& p)
{
  return p.cell;
}

cuboidCell cuboidPartition::getCuboidCell()
{
  return cell;
}

cuboidCellExt cuboidPartition::getCuboidCellExt(size_t index) const
{
  return cuboidCellExt(cell,vec_ptr[index]->second);
}

Rcpp::List cuboidPartition::toList() const
{
  Rcpp::List list;
  for(auto it = vec_ptr.begin(); it != vec_ptr.end();it++)
  {
    list.push_back(cuboidCellExt((*it)->first,(*it)->second).toList());
  }
  return list;
}
