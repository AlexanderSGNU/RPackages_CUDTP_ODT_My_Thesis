#include "../inst/include/cuboidCellkmax.h"

cuboidCellkmax::cuboidCellkmax(const cuboidCell& c, size_t k): cell(cuboidCell(c)), kmax(k){}

cuboidCellkmax::cuboidCellkmax(const size_tVectorkmax& vec):cell(cuboidCell(vec.vec,vec.vec)),kmax(vec.kmax){}

bool cuboidCellkmax::operator== (const cuboidCellkmax& cC) const
{
  return cell==cC.cell;
}
