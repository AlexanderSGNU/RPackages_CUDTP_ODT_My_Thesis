#include "../inst/include/cuboidCellExt.h"

// cuboidCellExt = list(vecLowInd,vecHighInd,vecnby,scalarCost)

cuboidCellExt::cuboidCellExt(){}

cuboidCellExt::cuboidCellExt(const size_tVector& lInd, const size_tVector& hInd, const size_tVector& nby, double cost) :
  cell(cuboidCell(lInd,hInd)), ext(cuboidExt(nby, cost))
  {}

cuboidCellExt::cuboidCellExt(const cuboidCell& cub, const size_tVector& nby, double cost) :
  cell(cub), ext(cuboidExt(nby, cost))
  {}

cuboidCellExt::cuboidCellExt(const cuboidCell& cub, const cuboidExt& ext) :
  cell(cub), ext(ext)
  {}

cuboidCellExt::cuboidCellExt(const cuboidCellkmax& cub, const cuboidExt& ext) :
  cell(cub.cell), ext(ext)
  {}

size_t cuboidCellExt::getDimension() const
{
  return cell.getDimension();
}

size_t cuboidCellExt::getCellVolume() const
{
  return cell.getCellVolume();
}

Rcpp::List cuboidCellExt::toList() const
{
  return Rcpp::List::create(cell.lowIndices.ToIntegerVector(),
                            cell.highIndices.ToIntegerVector(),
                            ext.nbyValues.ToIntegerVector(),
                            ext.cost);
}

Rcpp::List cuboidCellExt::toList(const cuboidCellExt& cub)
{
  return Rcpp::List::create(cub.cell.lowIndices.ToIntegerVector(),
                            cub.cell.highIndices.ToIntegerVector(),
                            cub.ext.nbyValues.ToIntegerVector(),
                            cub.ext.cost);
}
