#include "../inst/include/cuboidCell.h"

cuboidCell::cuboidCell(){}

cuboidCell::cuboidCell(const size_tVector& lInd, const size_tVector& hInd): lowIndices(size_tVector(lInd)), highIndices(size_tVector(hInd)) {}

cuboidCell::cuboidCell(const cuboidCell& cC)
{
  lowIndices = size_tVector(cC.lowIndices);
  highIndices = size_tVector(cC.highIndices);
}

cuboidCell& cuboidCell::operator= (const cuboidCell& cC)
{
  lowIndices = size_tVector(cC.lowIndices);
  highIndices = size_tVector(cC.highIndices);
  return *this;
}

bool cuboidCell::operator== (const cuboidCell& cC) const
{
  return (lowIndices==cC.lowIndices && highIndices==cC.highIndices);
}

size_t cuboidCell::getDimension() const
{
  return lowIndices.size();
}

size_t cuboidCell::getCellVolume() const
{
  return productVectorElements(highIndices - lowIndices);
}

Rcpp::List cuboidCell::toList() const
{
  return Rcpp::List::create(lowIndices.ToIntegerVector(),highIndices.ToIntegerVector());
}

Rcpp::List cuboidCell::toList(const cuboidCell& cub)
{
  return Rcpp::List::create(cub.lowIndices.ToIntegerVector(),cub.highIndices.ToIntegerVector());
}

// only for neighbours

cuboidCell cuboidCell::concatenateCells(const cuboidCell& c2) const
{
  if(c2.lowIndices < lowIndices)
  {
    return cuboidCell(c2.lowIndices,highIndices);
  }
  return cuboidCell(lowIndices,c2.highIndices);
}

size_t cuboidCell::productVectorElements(const size_tVector& vec)
{
  size_t product = 1;
  for(size_t i = 0; i < vec.size();i++)
  {
    product *= vec(i);
  }
  return product;
}
