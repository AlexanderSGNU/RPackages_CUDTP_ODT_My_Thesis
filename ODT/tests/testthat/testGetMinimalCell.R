test_that("getMinimalCell",{
  expect_equal(testGetMinimalCell(c(0.1,0.2),2,2),list(c(0,0)))
  expect_equal(testGetMinimalCell(c(0.1,0.2),2,4),list(c(1,3)))
  expect_equal(testGetMinimalCell(c(1,1),2,4),list(c(15,15)))
  expect_equal(testGetMinimalCell(c(0.5,0.2),2,4),list(c(8,3)))
  expect_equal(testGetMinimalCell(c(0.5,0.2,0.3),3,1),list(c(1,0,0)))
  expect_equal(testGetMinimalCell(c(0.5,0.2,0.3,0.7),4,4),list(c(8,3,4,11)))
})
